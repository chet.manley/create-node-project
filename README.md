# @chet.manley/create-node-project

> **Quickly generate new node projects from templates.**

[![project vulnerabilities][vulnerabilities-badge]][vulnerabilities-url]
[![project dependencies][dependencies-badge]][dependencies-url]
[![code style standardjs][standardjs-badge]][standardjs-url]
[![versioning strategy][semver-badge]][semver-url]
[![required Node version][node-version-badge]][node-version-url]

[![CI pipeline status][ci-badge]][ci-url]
[![code coverage][coverage-badge]][coverage-url]

<!-- badge images and URLs -->
[ci-badge]: https://gitlab.com/chet.manley/create-node-project/badges/master/pipeline.svg
[ci-url]: https://gitlab.com/chet.manley/create-node-project
[coverage-badge]: https://gitlab.com/chet.manley/create-node-project/badges/master/coverage.svg?job=Code%20Coverage
[coverage-url]: https://chet.manley.gitlab.io/create-node-project/master/coverage
[dependencies-badge]: https://img.shields.io/librariesio/release/npm/@chet.manley/create-node-project?logo=&style=for-the-badge
[dependencies-url]: https://www.npmjs.com/package/@chet.manley/create-node-project?activeTab=dependencies
[node-version-badge]: https://img.shields.io/node/v/@chet.manley/create-node-project?logo=&style=for-the-badge
[node-version-url]: https://nodejs.org/en/about/releases/
[semver-badge]: https://img.shields.io/static/v1?label=semver&message=standard-version&color=brightgreen&logo=&style=for-the-badge
[semver-url]: https://github.com/conventional-changelog/standard-version
[standardjs-badge]: https://img.shields.io/static/v1?label=style&message=standardJS&color=brightgreen&logo=&style=for-the-badge
[standardjs-url]: https://standardjs.com/
[vulnerabilities-badge]: https://img.shields.io/snyk/vulnerabilities/npm/@chet.manley/create-node-project?logo=&style=for-the-badge
[vulnerabilities-url]: https://snyk.io/vuln/search?q=%40chet.manley%2Fcreate-node-project&type=npm

## Releases

[![latest release version][stable-release-badge]][npm-url]
[![next release version][next-release-badge]][npm-url]
[![package install size][install-size-badge]][install-size-url]

<!-- badge images and URLs -->
[install-size-badge]: https://flat.badgen.net/packagephobia/publish/@chet.manley/create-node-project
[install-size-url]: https://packagephobia.now.sh/result?p=@chet.manley/create-node-project
[next-release-badge]: https://img.shields.io/npm/v/@chet.manley/create-node-project/next?label=@next&logo=npm&style=flat-square
[npm-url]: https://npmjs.org/package/@chet.manley/create-node-project
[stable-release-badge]: https://img.shields.io/npm/v/@chet.manley/create-node-project/latest?label=%40latest&logo=npm&style=flat-square

---

<!-- [[_TOC_]] -->
- [@chet.manley/create-node-project](#chetmanleycreate-node-project)
  - [Releases](#releases)
  - [Quick Start](#quick-start)
    - [Dependencies](#dependencies)
    - [Install](#install)
    - [Usage](#usage)
    - [Positional Arguments](#positional-arguments)
      - [`<template name>`](#template-name)
      - [`<project name>`](#project-name)
    - [Flags](#flags)
      - [User Config](#user-config)
      - [Disable Git](#disable-git)
      - [Disable NPM Install](#disable-npm-install)
      - [Repository](#repository)
      - [Target Directory](#target-directory)
      - [Templates Directory](#templates-directory)
      - [Update Package](#update-package)
      - [Update All Template Packages](#update-all-template-packages)
      - [Verbose](#verbose)
      - [Accept Defaults](#accept-defaults)
  - [Examples](#examples)
    - [Interactive Mode](#interactive-mode)
    - [Interactive Mode With Options](#interactive-mode-with-options)
    - [Apply Defaults](#apply-defaults)
    - [Set Target Directory](#set-target-directory)
    - [Install in Current Directory](#install-in-current-directory)
    - [Load User Config](#load-user-config)
  - [User Defined Templates](#user-defined-templates)
  - [Quick Reference](#quick-reference)
    - [Positional Arguments](#positional-arguments-1)
    - [Options](#options)
  - [Built with](#built-with)
  - [Contributing](#contributing)
  - [License](#license)

## Quick Start

### Dependencies

> :warning:\
> **This package requires a properly configured Git installation in order to function properly.
> It has been tested with Git version 2.26, but lower versions are very likely to work as well.**

### Install

> :information_source:\
> **_This package is meant to be used as an npm initializer -- no explicit installation is required._**

### Usage

```shell
npm init @chet.manley/node-project
# or
npx @chet.manley/create-node-project
```

> :information_source:\
> **Running with no arguments will enter interactive mode, rendering a menu that requests the minimum required options.
> The rest of the configuration will then be calculated from the information you provide.**

### Positional Arguments

#### `<template name>`

> `default: 'base'`\
> `type: positional #0`\
> `options: base, cjs, cli, es6, ts`
>
> Options listed are the templates provided by
> [@chet.manley/node-project-templates](https://gitlab.com/chet.manley/node-project-templates).
> If using your own templates via the [`--templates-dir` flag](#templates-directory), you will be able to choose from those instead.
>
> ```shell
> npm init @chet.manley/node-project cjs
> ```

#### `<project name>`

> `default: $CWD basename`\
> `type: positional #1`\
> `options: @project-namespace/project-name, project-name`
>
> Sets the `name` field in `package.json`, as well as the target directory
> (See the [`--target-dir` flag](#target-directory) for more information).
> Names containing spaces will be transformed to use dashes
> (E.g., `"my new project" => "my-new-project"`).
> Names are also lowercased, per the
> [npm documentation](https://docs.npmjs.com/cli/v6/configuring-npm/package-json#name).
>
> ```shell
> npm init @chet.manley/node-project cjs my-new-project
> ```

### Flags

#### User Config

> `flags: -c, --config`\
> `type: string`\
> `default: none`
>
> Provide a path to a configuration file that will override the defaults.
> `require` is used to load the file, so it must be a `.json`,
> or a `.js` that exports a configuration object.
> Config file can be any arbitrary name.
>
> ```shell
> npm init @chet.manley/node-project -c path/to/my-config.json
> ```
>
> > :information_source:\
> > If a relative path is provided, file location will be resolved in the following order:
> >
> > - `$CWD/`
> > - `~/`
> > - `~/.create-node-project/`
> > - `~/.config/`
> > - `~/.config/create-node-project/`
>
> ##### Available Options
>
> | key            | type    | default | description |
> | :---           | :---:   | :---    | :---        |
> | checkoutBranch | string  | `"integration"` | Branch to checkout after initial commit (Empty string  or `null` to remain on master) |
> | commitMessage  | string  | `"\"chore: initial commit :feelsgood:\""` | Initial commit message |
> | gitInit        | boolean | `true` | [`--git-init` flag](#disable-git) |
> | npmInstall     | boolean | `true` | [`--npm-install` flag](#disable-npm-install) |
> | paths          | array   | `["~/", "~/.create-node-project", "~/.config", "~/.config/create-node-project"]` | Default paths to search (Currently only used when loading user templates) |
> | projectName    | string  | `$CWD basename` | [`<project name> positional`](#project-name) |
> | repository     | string  | - | [`--repository` flag](#repository) |
> | targetDir      | string  | `$CWD` | [`--target-dir` flag](#target-directory) |
> | template       | string  | - | [`<template name>` positional](#template-name) |
> | templatesDir   | string  | - | [`--templates-dir` flag](#templates-directory) |
> | update         | boolean | `false` | [`--update` flag](#update-package) |
> | updateAll      | boolean | `false` | [`--update-all` flag](#update-all-template-packages) |
> | yes            | boolean | `false` | [`--yes` flag](#accept-defaults) |

#### Disable Git

> `flags: --no-git-init`\
> `type: boolean`\
> `default: false`
>
> Do not initialize a new git repository for this project.
>
> ```shell
> npm init @chet.manley/node-project --no-git-init
> ```

#### Disable NPM Install

> `flags: --no-npm-install`\
> `type: boolean`\
> `default: false`
>
> Do not run `npm install` after installing template files.
>
> ```shell
> npm init @chet.manley/node-project --no-npm-install
> ```

#### Repository

> `flags: -r, --repository`\
> `type: string`\
> `default: none`
>
> URL pointing to empty remote repository of the same project name.
> This adds a Git remote origin, as well as filling the appropriate fields in `package.json`.
>
> ```shell
> npm init @chet.manley/node-project \
> -r "https://gitlab.com/my-namespace/my-new-project.git"
> ```

#### Target Directory

> `flags: -t, --target-dir`\
> `type: string`\
> `default: $CWD | $CWD/<project name>`
>
> Where you want your new project to be installed.
> If a relative path is provided, the computed target will be relative to your current working directory.
>
> If the basename (E.g., `my-target` of `/home/username/projects/my-target`) of the computed target path differs from the project name, the project will be created in a project-named subdirectory of the target (E.g., `/home/username/projects/my-target/my-new-project`).
>
> The default behavior will create a project-named subdirectory in your current working directory if your CWD differs from the project name.
>
> ```shell
> npm init @chet.manley/node-project -t ~/projects/my-target
> ```

#### Templates Directory

> `flags: --templates-dir`\
> `type: string`\
> `default: none`
>
> Load user-defined templates from this directory.
> If a relative path is provided, the computed path will be relative to your current working directory.
> Browse the [@chet.manley/node-project-templates][node-project-templates] project for details regarding implementing your own templates.
>
> The default behavior loads the templates provided by this package.
>
> ```shell
> npm init @chet.manley/node-project \
> --templates-dir path/to/my-templates
> ```
>
> > :information_source:\
> > If a relative path is provided, directory location will be resolved in the following order (this can be overriden using the [`--config flag`](#user-config)):
> >
> > - `$CWD/`
> > - `~/`
> > - `~/.create-node-project/`
> > - `~/.config/`
> > - `~/.config/create-node-project/`

#### Update Package

> `flags: -u, --update`\
> `type: boolean`\
> `default: false`
>
> Check the selected template's `package.json` for dependency updates and apply them before the `npm install` step.
>
> ```shell
> npm init @chet.manley/node-project -u
> ```

#### Update All Template Packages

> `flags: -U, --update-all`\
> `type: boolean`\
> `default: false`
>
> If you have installed this package globally, this will check the `package.json` of each template for dependency updates, apply available updates, then exit.
> Can also be combined with the [`--templates-dir` flag](#templates-directory) to update user-defined templates.
>
> ```shell
> create-node-project -U
> # or
> npm init @chet.manley/node-project -U \
> [--templates-dir path/to/my-templates]
> ```

#### Verbose

> `flags: -V, --verbose`\
> `type: count`\
> `default: 0`
>
> Set the output verbosity of the program.
>
> ```shell
> npm init @chet.manley/node-project -VVV
> ```

#### Accept Defaults

> `flags: -y, --yes`\
> `type: boolean`\
> `default: false`
>
> Explicitly enter non-interactive mode, accepting defaults with no prompts.
>
> ```shell
> npm init @chet.manley/node-project -y
> ```

## Examples

### Interactive Mode

```shell
npm init @chet.manley/node-project
```

- Prompts for minimum required options.

### Interactive Mode With Options

```shell
npm init @chet.manley/node-project cjs my-new-project
```

- Creates a new project named `my-new-project` using `cjs` template.
- Prompts for any missing options.

### Apply Defaults

```shell
npm init @chet.manley/node-project cjs my-new-project -y
```

- Creates a new project named `my-new-project` using `cjs` template.
- Applies defaults for missing options.

### Set Target Directory

```shell
npm init @chet.manley/node-project cjs my-new-project -y \
--target-dir projects
```

- Creates a new project named `my-new-project` using `cjs` template.
- Applies defaults for missing options.
- Installs to `$CWD/projects/my-new-project`.

### Install in Current Directory

```shell
mkdir ./my-new-project
cd ./my-new-project
npm init @chet.manley/node-project cjs my-new-project
```

- Creates a new project named `my-new-project` using `cjs` template.
- Prompts for any missing options.
- Installs to `$CWD`.

### Load User Config

> `~/projects/my-new-project.json`:
>
> ```json
> {
>   "projectName": "my-new-project",
>   "repository": "https://gitlab.com/name.space/my-new-project.git",
>   "targetDir": "~/projects",
>   "template": "mytmplname",
>   "templatesDir": "~/projects/my-templates",
>   "update": true
> }
> ```

```shell
cd ~/
npm init @chet.manley/node-project -c projects/my-new-project.json
```

- Loads user config from `/home/<username>/projects/my-new-project.json`.
- Loads user templates from `/home/<username>/projects/my-templates/`.
- Creates a new project named `my-new-project` using `mytmplname` template.
- Installs to `/home/username/projects/my-new-project/`.
- Updates package dependencies.
- Adds remote origin URL.

## User Defined Templates

> :information_source:\
> See the [@chet.manley/node-project-templates][node-project-templates] repository for information regarding creating your own templates.

## Quick Reference

### Positional Arguments

| Name          | Position |     Default     | Short Description         |
| :------------ | :------: | :-------------: | :------------------------ |
| template name |    0     |    `'base'`     | Name of template to apply |
| project name  |    1     | `$CWD basename` | Name of project           |

### Options

| Flags              | Type    | Default | Short Description                        |
| :----------------- | :------ | :-----: | :--------------------------------------- |
| `-c, --config`     | string  |    -    | path/to/user-config.js[on]               |
| `--no-git-init`    | boolean | `false` | Do not init Git repo                     |
| `--no-npm-install` | boolean | `false` | Do not run `npm install`                 |
| `-r, --repository` | string  |    -    | Add Git remote repo URL                  |
| `-t, --target-dir` | string  | `$CWD`  | path/to/target/dir                       |
| `--templates-dir`  | string  |    -    | path/to/user/templates                   |
| `-u, --update`     | boolean | `false` | Update dependencies before `npm install` |
| `-U, --update-all` | boolean | `false` | Update dependencies for all templates, then exit |
| `-V, --verbose`    | count   | `0`     | Set output verbosity                     |
| `-y, --yes`        | boolean | `false` | Accept defaults                          |

## Built with

[![Fedora Linux](https://img.shields.io/static/v1?logo=fedora&label=Fedora&message=workstation&color=294172&style=flat)](https://getfedora.org/)
[![VSCode](https://img.shields.io/static/v1?logo=visual-studio-code&label=VSCode&message=insiders&color=2A917D&style=flat)](https://code.visualstudio.com/)
[![GitLab](https://img.shields.io/static/v1?logo=gitlab&label=GitLab&message=FOSS&color=DE4020&style=flat)](https://gitlab.com/gitlab-org/gitlab)
[![Caffeine](https://img.shields.io/static/v1?logo=buy-me-a-coffee&label=Caffeine&message=☕&color=603015&style=flat)](https://en.wikipedia.org/wiki/Caffeine)

## Contributing

The community is welcome to participate in this open source project.
Aspiring contributors should review the [contributing guide](https://gitlab.com/chet.manley/create-node-project/-/blob/master/CONTRIBUTING.md) for details on how to get started.
First-time contributors are encouraged to search for issues with the ~"good first issue" label.

## License

[![NPM][license-badge]][license-url]

Copyright © 2020 [Chet Manley](https://gitlab.com/chet.manley).

<!-- badge images and URLs -->
[license-badge]: https://img.shields.io/npm/l/@chet.manley/create-node-project
[license-url]: https://gitlab.com/chet.manley/create-node-project/-/blob/master/LICENSE

<!-- ## Acknowledgements -->

<!-- other URLs -->
[node-project-templates]: https://gitlab.com/chet.manley/node-project-templates/
