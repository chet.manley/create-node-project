'use strict'

const { createShellCommand } = require('../utils')

function createNPMCommands (targetPath) {
  const npm = createShellCommand('npm', targetPath)

  return {
    auditFix: npm('audit', 'fix'),
    init: npm('init', '-y', '--silent'),
    install: npm('install')
  }
}

module.exports = exports = createNPMCommands
