'use strict'

function validateConfig ({
  projectName,
  template,
  templateName
}) {
  if (!projectName) {
    throw new Error('Cannot calculate default project name from root')
  }

  if (template === null) {
    throw new Error(`Template '${templateName}' not found in templates`)
  }

  return true
}

module.exports = exports = validateConfig
