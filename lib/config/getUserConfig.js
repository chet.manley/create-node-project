'use strict'

const { calculatePaths } = require('../utils/fs')

function getUserConfig (configFile, defaultPaths) {
  const paths = calculatePaths(configFile, defaultPaths)

  for (const path of paths) {
    try {
      return require(path)
    } catch (error) {
      if (error.code === 'EACCES') {
        throw new Error('Permission denied accessing user config')
      }
      if (error.code !== 'MODULE_NOT_FOUND') { throw error }
    }
  }

  throw new Error('User config not found')
}

module.exports = exports = getUserConfig
