'use strict'

const yargs = require('yargs')

const createYargsConfig = require('./createYargsConfig')

function createOptionsFromArgs (args, defaults) {
  const { options, positionals, usage } = createYargsConfig(defaults)

  return yargs(args)
    .usage(
      usage.command,
      usage.description,
      yargs => yargs
        .positional('template-name', positionals.templateName)
        .positional('project-name', positionals.projectName)
    )
    .options(options)
    .strict()
    .fail((msg, err) => {
      throw err || new Error(msg)
    })
    .argv
}

module.exports = exports = createOptionsFromArgs
