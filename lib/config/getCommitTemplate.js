'use strict'

function getCommitTemplate (installTree) {
  return installTree.reduce(
    (cT, tmpl) => tmpl.config.commitTemplate || cT,
    ''
  )
}

module.exports = exports = getCommitTemplate
