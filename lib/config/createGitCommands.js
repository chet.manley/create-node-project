'use strict'

const { createShellCommand } = require('../utils')

function createGitCommands (targetPath) {
  const git = createShellCommand('git', targetPath)

  return {
    addAll: git('add', '.'),
    addOrigin: git('remote', 'add', 'origin'),
    checkout: {
      branch: git('checkout', '-b')
    },
    commit: git('commit', '-m'),
    config: {
      get: git('config', '--get'),
      getRegexp: git('config', '--get-regexp'),
      set: git('config', '--add')
    },
    init: git('init'),
    push: {
      branch: git('push', '--set-upstream', 'origin')
    }
  }
}

module.exports = exports = createGitCommands
