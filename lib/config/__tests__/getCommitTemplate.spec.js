'use strict'

const getCommitTemplate = require('../getCommitTemplate.js')

const mockCommitTemplate = '.commit.final.template'
const mockTemplate1 = { config: { commitTemplate: '.commit.template' } }
const mockTemplate2 = { config: { commitTemplate: mockCommitTemplate } }
const mockTemplate3 = { config: {} }
const mockInstallTree = [mockTemplate1, mockTemplate2, mockTemplate3]

describe('getCommitTemplate Function', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should return empty String if no commit template found', () => {
    expect(getCommitTemplate([])).toBe('')
  })

  it('should return commit template file name as String', () => {
    expect(getCommitTemplate(mockInstallTree)).toEqual(mockCommitTemplate)
  })
})
