'use strict'

const prompt = require('../prompt.js')

const mockQuestions = [
  { name: 'mockName' },
  { name: 'mock-template' }
]
const mockAnswers = {
  mockName: 'mock answer',
  'mock-template': 'mock answer'
}

jest.spyOn(console, 'log').mockImplementation(_ => {})

describe('prompt AsyncFunction', () => {
  it('should reject with Error if missing argument', async () => {
    await expect(prompt()).rejects.toThrow(Error)
  })

  it('should resolve with empty Object', async () => {
    await expect(prompt([])).resolves.toEqual({})
  })

  it('should resolve with Object of answered questions', async () => {
    await expect(prompt(mockQuestions)).resolves.toEqual(mockAnswers)
  })
})
