'use strict'

const mockSpawn = require('child_process').spawn

const createGitCommands = require('../createGitCommands.js')

const gitCommandsObject = require('../__data__/mockConfig.js').commands.git

const mockCWD = '/mock-cwd'
const expectedCommands = [
  'addAll',
  'addOrigin',
  'checkout.branch',
  'commit',
  'config.get',
  'config.getRegexp',
  'config.set',
  'init',
  'push.branch'
]

function getCommandFromPath (object, path) {
  return path
    .split('.')
    .reduce((o, k) => o && o[k], object)
}

describe('createGitCommands Function', () => {
  const commands = createGitCommands(mockCWD)

  it('should return only expected Git commands', () => {
    expect(commands).toEqual(gitCommandsObject)
  })

  it.each(expectedCommands)(
    'should provide #%s() method',
    (command) => {
      expect(commands).toHaveProperty(command, expect.any(Function))
    }
  )

  it.each(expectedCommands)(
    'should spawn %s command with defined CWD',
    (path) => {
      getCommandFromPath(commands, path)()

      expect(mockSpawn).toHaveBeenLastCalledWith(
        'git',
        expect.any(Array),
        expect.objectContaining({ cwd: mockCWD })
      )
    }
  )
})
