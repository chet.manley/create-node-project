'use strict'

const createOptionsFromArgs = require('../createOptionsFromArgs.js')
const defaults = require('../defaults.js')

describe('createOptionsFromArgs Function', () => {
  it('should throw Error on unknown arguments', () => {
    expect(_ => createOptionsFromArgs(
      ['--mock-invalid'],
      defaults
    )).toThrow('Unknown arguments')
  })

  it('should apply defaults', () => {
    const expectedDefaults = {
      debug: defaults.debug,
      gitInit: defaults.gitInit,
      npmInstall: defaults.npmInstall,
      repository: defaults.repository,
      targetDir: defaults.targetDir,
      templatesDir: defaults.templatesDir,
      update: defaults.update,
      updateAll: defaults.updateAll,
      yes: defaults.yes
    }

    expect(createOptionsFromArgs([], defaults))
      .toEqual(expect.objectContaining(expectedDefaults))
  })

  it('should override defaults', () => {
    const cliArgs = [
      '-D',
      '--no-git-init',
      '--no-npm-install',
      '-r', 'https://gitlab.com/mock-repo.git',
      '-t', '/tmp/mock-project',
      '--templates-dir', '/tmp/templates',
      '-u',
      '-U',
      '-V',
      '-y'
    ]
    const expectedDefaultsOverride = {
      debug: !defaults.debug,
      gitInit: !defaults.gitInit,
      npmInstall: !defaults.npmInstall,
      repository: 'https://gitlab.com/mock-repo.git',
      targetDir: '/tmp/mock-project',
      templatesDir: '/tmp/templates',
      update: !defaults.update,
      updateAll: !defaults.updateAll,
      verbose: 1,
      yes: !defaults.yes
    }

    expect(createOptionsFromArgs(cliArgs, defaults))
      .toEqual(expect.objectContaining(expectedDefaultsOverride))
  })

  it('should accept positional CLI arguments', () => {
    const projectName = 'mock-project'
    const templateName = 'mock-template'

    expect(createOptionsFromArgs(
      [templateName, projectName],
      defaults
    )).toEqual(expect.objectContaining({
      projectName,
      templateName
    }))
  })
})
