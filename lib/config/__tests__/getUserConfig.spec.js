'use strict'

const { join } = require('path')

const getUserConfig = require('../getUserConfig.js')

const mockDefaultPaths = ['/tmp']
const mockConfigFile = 'config.json'
const mockConfigModule = join(mockDefaultPaths[0], mockConfigFile)
const mockConfig = { key1: 'value1' }
const mockError = new Error('mock error')

describe('getUserConfig Function', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    jest.doMock(mockConfigModule, _ => mockConfig, { virtual: true })
  })

  afterEach(() => {
    mockError.code = undefined
    jest.resetModules()
  })

  afterAll(() => {
    jest.unmock(mockConfigModule)
  })

  it('should throw Error if user config not found', () => {
    expect(_ => getUserConfig('/mockdir', mockDefaultPaths))
      .toThrow(Error)
  })

  it('should throw Error if access is denied', () => {
    mockError.code = 'EACCES'
    jest.doMock(
      mockConfigModule,
      _ => { throw mockError },
      { virtual: true }
    )

    expect(_ => getUserConfig(mockConfigModule, mockDefaultPaths))
      .toThrow('Permission denied accessing user')
  })

  it('should rethrow errors thrown by require()', () => {
    mockError.code = 'EMOCK'
    jest.doMock(
      mockConfigModule,
      _ => { throw mockError },
      { virtual: true }
    )

    expect(_ => getUserConfig(mockConfigModule, mockDefaultPaths))
      .toThrow(mockError)
  })

  it('should return user config Object', () => {
    expect(getUserConfig(mockConfigModule))
      .toEqual(mockConfig)
    expect(getUserConfig(mockConfigFile, mockDefaultPaths))
      .toEqual(mockConfig)
  })
})
