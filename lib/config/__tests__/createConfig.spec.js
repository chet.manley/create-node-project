'use strict'

jest.mock('../createGitCommands.js')

const mockPrompt = require('inquirer').prompt

const createConfig = require('../createConfig.js')

const defaults = { ...require('../defaults.js') }
const mockConfig = require('../__data__/mockConfig.js')

const mockUserConfig = {
  repository: 'https://mock.site/mock-repo.git',
  update: true
}
const mockUserConfigModule = '/tmp/user-config.js'

const ENV = JSON.parse(JSON.stringify(process.env))

describe('createConfig AsyncFactory', () => {
  jest.spyOn(console, 'log').mockImplementation(_ => {})

  beforeEach(() => {
    process.env = { ...ENV }
    jest.clearAllMocks()
    mockPrompt.mockResolvedValueOnce({ templateName: 'base' })
  })

  afterEach(() => mockPrompt.mockReset())

  it('should resolve to config Object', async () => {
    await expect(createConfig([])).resolves.toEqual(mockConfig)
  })

  it('should merge defaults into config', async () => {
    const config = await createConfig([])

    for (const setting of Object.entries(defaults)) {
      expect(config).toHaveProperty(...setting)
    }
  })

  it('should set process-wide debug', async () => {
    const config = await createConfig(['-D'])

    expect(config).toHaveProperty('debug', true)
    expect(config).toHaveProperty('verbose', 10)
    expect(process.env.DEBUG).toBeTruthy()
  })

  it('should merge user config into main config', async () => {
    jest.doMock(mockUserConfigModule, _ => mockUserConfig, { virtual: true })

    await expect(createConfig(['-c', mockUserConfigModule]))
      .resolves.toEqual(expect.objectContaining(mockUserConfig))

    jest.unmock(mockUserConfigModule)
  })

  it('should normalize project name', async () => {
    const mockProjectName = 'mock projectName'
    const normalizedProjectName = 'mock-projectname'

    await expect(createConfig(['', mockProjectName]))
      .resolves.toHaveProperty('projectName', normalizedProjectName)
  })

  it('should resolve early with -U flag', async () => {
    const config = await createConfig(['-U'])

    expect(mockPrompt).not.toHaveBeenCalled()
    expect(config).toHaveProperty('updateAll', true)
    expect(config).not.toHaveProperty('commands')
    expect(config).not.toHaveProperty('commitTemplate')
    expect(config).not.toHaveProperty('installTree')
    expect(config).not.toHaveProperty('targetPath')
  })

  it('should skip prompting for options with -y flag', async () => {
    const config = await createConfig(['base', 'mock-project', '-y'])

    expect(mockPrompt).not.toHaveBeenCalled()
    expect(config).toHaveProperty('projectName', 'mock-project')
    expect(config).toHaveProperty('templateName', 'base')
    expect(config).toHaveProperty('template.name', 'base')
  })

  it('should reject with Error on bad template name', async () => {
    expect(mockPrompt).not.toHaveBeenCalled()
    await expect(createConfig(['mock-bad-template', '-y']))
      .rejects.toThrow('mock-bad-template')
  })

  it('should reject with Error on invalid flags', async () => {
    await expect(createConfig(['--mock-bad-flag']))
      .rejects.toThrow('mock-bad-flag')
  })
})
