'use strict'

const mockSpawn = require('child_process').spawn

const createNPMCommands = require('../createNPMCommands.js')

const npmCommandsObject = require('../__data__/mockConfig.js').commands.npm

const mockCWD = '/mock-cwd'
const expectedCommands = [
  'auditFix',
  'init',
  'install'
]

describe('createNPMCommands Function', () => {
  const commands = createNPMCommands(mockCWD)

  it('should return only expected NPM commands', () => {
    expect(commands).toEqual(npmCommandsObject)
  })

  it.each(expectedCommands)(
    'should provide #%s() method',
    (command) => {
      expect(commands).toHaveProperty(command, expect.any(Function))
    }
  )

  it.each(expectedCommands)(
    'should spawn %s command with defined CWD',
    (command) => {
      commands[command]()

      expect(mockSpawn).toHaveBeenLastCalledWith(
        'npm',
        expect.any(Array),
        expect.objectContaining({ cwd: mockCWD })
      )
    }
  )
})
