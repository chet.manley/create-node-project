'use strict'

const normalizeName = require('../normalizeName.js')

describe('normalizeName Function', () => {
  it('should return empty string if missing argument', () => {
    expect(normalizeName()).toBe('')
  })

  it('should trim leading & trailing whitepace', () => {
    expect(normalizeName(' \tmock-string \t')).toBe('mock-string')
  })

  it('should replace spaces with dashes', () => {
    expect(normalizeName('mock string')).toBe('mock-string')
  })

  it('should lowercase entire string', () => {
    /**
     * https://docs.npmjs.com/cli/v6/configuring-npm/package-json#name
     */

    expect(normalizeName('Mock Project Name')).toBe('mock-project-name')
  })
})
