'use strict'

const createInstallTree = require('../createInstallTree.js')

const mockTemplate1 = { name: 'base', config: {} }
const mockTemplate2 = { name: 'tmpl1', config: { dependencyName: 'base' } }
const mockTemplate3 = { name: 'tmpl2', config: { dependencyName: 'tmpl1' } }
const mockTemplateMissing = { name: 'tmpl2', config: { dependencyName: 'a' } }
const mockTemplateRecursive = {
  name: 'tmpl1',
  config: {
    dependencyName: 'tmpl2'
  }
}
const mockTemplates = {
  list: [
    mockTemplate1,
    mockTemplate2,
    mockTemplate3
  ],
  getTemplate (template = '') {
    return this.list.find(({ name }) => name === template)
  }
}
const mockTemplatesRecursive = {
  list: [
    mockTemplateRecursive,
    mockTemplate3
  ],
  getTemplate (template = '') {
    return this.list.find(({ name }) => name === template)
  }
}

const mockInstallTree = [mockTemplate1, mockTemplate2, mockTemplate3]

describe('getInstallTree Function', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should throw Error if dependency is missing', () => {
    expect(_ => createInstallTree(
      mockTemplateMissing,
      mockTemplates
    )).toThrow('missing dependency')
  })

  it('should throw Error if dependency is recursive', () => {
    expect(_ => createInstallTree(
      mockTemplate3,
      mockTemplatesRecursive
    )).toThrow('circular dependency')
  })

  it('should return install tree as Template[]', () => {
    expect(createInstallTree(
      mockTemplate3,
      mockTemplates
    )).toEqual(mockInstallTree)
  })
})
