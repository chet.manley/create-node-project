'use strict'

const getMissingOptions = require('../getMissingOptions.js')

const mockOptions = {
  gitInit: false,
  projectName: 'mock-project-name',
  repository: '',
  templateName: 'mock-template',
  templates: {
    getTemplate: jest.fn(_ => 'mock-template')
  },
  get template () {
    return this.templates.getTemplate(this.templateName)
  }
}
const mockDefaults = {
  projectName: 'mock-default-project-name'
}

jest.spyOn(console, 'log').mockImplementation(_ => {})

describe('getMissingOptions AsyncFunction', () => {
  it('should resolve to empty Object if no options are missing', async () => {
    await expect(getMissingOptions(mockOptions, mockDefaults))
      .resolves.toEqual({})
  })

  it('should resolve with template name', async () => {
    const mockAnswer = { templateName: 'mock answer' }

    mockOptions.templates.getTemplate.mockReturnValueOnce(null)

    await expect(getMissingOptions(mockOptions, mockDefaults))
      .resolves.toEqual(mockAnswer)
  })

  it('should resolve with normalized project name', async () => {
    const mockAnswer = { projectName: 'mock-answer' }

    await expect(getMissingOptions(
      { ...mockOptions, projectName: '' },
      mockDefaults
    )).resolves.toEqual(mockAnswer)
  })

  it('should resolve with repository', async () => {
    const mockAnswer = { repository: 'mock answer' }

    await expect(getMissingOptions(
      { ...mockOptions, gitInit: true },
      mockDefaults
    )).resolves.toEqual(mockAnswer)
  })
})
