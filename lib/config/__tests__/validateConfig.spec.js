'use strict'

const validateConfig = require('../validateConfig.js')

const mockConfig = {
  projectName: 'mock-project',
  templateName: 'mock-template',
  templates: {
    getTemplate: jest.fn(_ => true)
  },
  get template () {
    return this.templates.getTemplate(this.templateName)
  }
}
const mockInvalidConfig = {
  ...mockConfig,
  projectName: ''
}

describe('validateConfig Function', () => {
  it('should throw Error on missing project name', () => {
    expect(_ => validateConfig(mockInvalidConfig)).toThrow(Error)
  })

  it('should throw Error on invalid template name', () => {
    mockConfig.templates.getTemplate.mockReturnValueOnce(null)

    expect(_ => validateConfig(mockConfig)).toThrow(Error)
  })

  it('should return true', () => {
    expect(validateConfig(mockConfig)).toBe(true)
  })
})
