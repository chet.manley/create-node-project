'use strict'

const createYargsConfig = require('../createYargsConfig.js')
const defaults = require('../defaults.js')

describe('createYargsConfig Function', () => {
  it('should throw TypeError if missing argument', () => {
    expect(_ => createYargsConfig()).toThrow(TypeError)
  })

  describe('should return Yargs config Object', () => {
    const config = createYargsConfig(defaults)

    describe('#options', () => {
      it('should provide #c, with alias', () => {
        expect(config.options)
          .toHaveProperty('c.alias', ['config'])
      })

      it('should provide #D, with alias, accepts default', () => {
        expect(config.options)
          .toHaveProperty('D.alias', ['debug'])
        expect(config.options)
          .toHaveProperty('D.default', defaults.debug)
      })

      it('should provide #git-init, accepts default', () => {
        expect(config.options)
          .toHaveProperty('git-init.default', defaults.gitInit)
      })

      it('should provide #npm-install, accepts default', () => {
        expect(config.options)
          .toHaveProperty('npm-install.default', defaults.npmInstall)
      })

      it('should provide #r, with alias, accepts default', () => {
        expect(config.options)
          .toHaveProperty('r.alias', ['repository'])
        expect(config.options)
          .toHaveProperty('r.default', defaults.repository)
      })

      it('should provide #t, with alias, accepts default', () => {
        expect(config.options)
          .toHaveProperty('t.alias', ['target-dir'])
        expect(config.options)
          .toHaveProperty('t.default', defaults.targetDir)
      })

      it('should provide #templates-dir, accepts default', () => {
        expect(config.options)
          .toHaveProperty('templates-dir.default', defaults.templatesDir)
      })

      it('should provide #u, with alias, accepts default', () => {
        expect(config.options)
          .toHaveProperty('u.alias', ['update'])
        expect(config.options)
          .toHaveProperty('u.default', defaults.update)
      })

      it('should provide #U, with alias', () => {
        expect(config.options)
          .toHaveProperty('U.alias', ['update-all'])
      })

      it('should provide #V, with alias', () => {
        expect(config.options)
          .toHaveProperty('V.alias', ['verbose'])
      })

      it('should provide #y, with alias, accepts default', () => {
        expect(config.options)
          .toHaveProperty('y.alias', ['yes'])
        expect(config.options)
          .toHaveProperty('y.default', defaults.yes)
      })
    })

    describe('#positionals', () => {
      it('should provide #projectName', () => {
        expect(config.positionals)
          .toHaveProperty('projectName')
      })

      it('should provide #templateName', () => {
        expect(config.positionals)
          .toHaveProperty('templateName')
      })
    })

    describe('#usage', () => {
      it('should provide #command as String', () => {
        expect(config.usage)
          .toHaveProperty('command', expect.any(String))
      })

      it('should provide #description as String', () => {
        expect(config.usage)
          .toHaveProperty('description', expect.any(String))
      })
    })
  })
})
