'use strict'

const defaults = require('../defaults.js')

const defaultProperties = [
  'checkoutBranch',
  'commitMessage',
  'debug',
  'gitInit',
  'npmInstall',
  'paths',
  'projectName',
  'repository',
  'targetDir',
  'templatesDir',
  'update',
  'updateAll',
  'yes'
]

describe('defaults Object', () => {
  it('should provide #checkoutBranch as String', () => {
    expect(defaults).toHaveProperty('checkoutBranch', expect.any(String))
  })

  it('should provide #commitMessage as String', () => {
    expect(defaults).toHaveProperty('commitMessage', expect.any(String))
  })

  it('should provide #debug as Boolean', () => {
    expect(defaults).toHaveProperty('debug', expect.any(Boolean))
  })

  it('should provide #gitInit as Boolean', () => {
    expect(defaults).toHaveProperty('gitInit', expect.any(Boolean))
  })

  it('should provide #npmInstall as Boolean', () => {
    expect(defaults).toHaveProperty('npmInstall', expect.any(Boolean))
  })

  it('should provide #paths as String[]', () => {
    expect(defaults).toHaveProperty(
      'paths',
      expect.arrayContaining([expect.any(String)])
    )
  })

  it('should provide #projectName as String', () => {
    expect(defaults).toHaveProperty('projectName', expect.any(String))
  })

  it('should provide #repository as String', () => {
    expect(defaults).toHaveProperty('repository', expect.any(String))
  })

  it('should provide #targetdir as String', () => {
    expect(defaults).toHaveProperty('targetDir', expect.any(String))
  })

  it('should provide #templatesDir as String', () => {
    expect(defaults).toHaveProperty('templatesDir', expect.any(String))
  })

  it('should provide #update as Boolean', () => {
    expect(defaults).toHaveProperty('update', expect.any(Boolean))
  })

  it('should provide #updateAll as Boolean', () => {
    expect(defaults).toHaveProperty('updateAll', expect.any(Boolean))
  })

  it('should provide #yes as Boolean', () => {
    expect(defaults).toHaveProperty('yes', expect.any(Boolean))
  })

  it('should provide only expected properties', () => {
    expect(Object.keys(defaults)).toEqual(defaultProperties)
  })
})
