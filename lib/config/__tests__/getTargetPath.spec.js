'use strict'

const { isAbsolute, join } = require('path')

const getTargetPath = require('../getTargetPath.js')

const mockProjectName = 'mock-project'
const mockTargetDir = 'mock-dir'
const mockTargetPath = join('/tmp', mockTargetDir)

describe('calculateTargetPath Function', () => {
  it('should throw TypeError if missing arguments', () => {
    expect(_ => getTargetPath()).toThrow()
    expect(_ => getTargetPath('path')).toThrow()
    expect(_ => getTargetPath(undefined, mockProjectName)).toThrow()
  })

  it('should throw Error on non-string arguments', () => {
    expect(_ => getTargetPath(null, mockProjectName)).toThrow()
    expect(_ => getTargetPath('/path', null)).toThrow()
  })

  it('should calculate an absolute path', () => {
    expect(isAbsolute(getTargetPath(mockTargetDir, mockProjectName)))
      .toBe(true)
    expect(isAbsolute(getTargetPath(mockTargetPath, mockProjectName)))
      .toBe(true)
  })

  it('should join target path with project name', () => {
    const matchingPath = join('/tmp', mockProjectName)

    expect(getTargetPath('/tmp', mockProjectName)).toBe(matchingPath)
    expect(getTargetPath(matchingPath, mockProjectName)).toBe(matchingPath)
  })
})
