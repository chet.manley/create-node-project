'use strict'

const mergeConfigObjects = require('../mergeConfigObjects.js')

describe('mergeConfigObjects Function', () => {
  it('should shallow merge two objects', () => {
    const mockConfig = { key1: 'value1' }
    const mockDefaults = { key2: 'value2' }
    const mergedObject = { ...mockConfig, ...mockDefaults }

    expect(mergeConfigObjects(mockConfig, mockDefaults))
      .toEqual(mergedObject)
  })

  it('should not overwite objects in config', () => {
    const mockConfig = { key1: 'value1' }
    const mockDefaults = { key1: 'value2' }
    const mergedObject = { ...mockConfig }

    expect(mergeConfigObjects(mockConfig, mockDefaults))
      .toEqual(mergedObject)
  })

  it('should overwite empty objects in config', () => {
    const mockConfig = { key1: '' }
    const mockDefaults = { key1: 'value2' }
    const mergedObject = { ...mockDefaults }

    expect(mergeConfigObjects(mockConfig, mockDefaults))
      .toEqual(mergedObject)
  })
})
