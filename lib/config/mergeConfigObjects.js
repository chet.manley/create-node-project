'use strict'

function predicate (val) {
  return val !== '' && val !== undefined
}

function mergeConfigObjects (config, toMerge) {
  for (const [key, value] of Object.entries(toMerge)) {
    predicate(config[key]) || (config[key] = value)
  }

  return config
}

module.exports = exports = mergeConfigObjects
