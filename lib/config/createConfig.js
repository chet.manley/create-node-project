'use strict'

const { getTemplates } = require('../templates')

const createGitCommands = require('./createGitCommands')
const createInstallTree = require('./createInstallTree')
const createNPMCommands = require('./createNPMCommands')
const createOptionsFromArgs = require('./createOptionsFromArgs')
const getCommitTemplate = require('./getCommitTemplate')
const getMissingOptions = require('./getMissingOptions')
const getTargetPath = require('./getTargetPath')
const getUserConfig = require('./getUserConfig')
const mergeConfigObjects = require('./mergeConfigObjects')
const normalizeName = require('./normalizeName')
const validateConfig = require('./validateConfig')

const defaults = require('./defaults')

async function createConfig (cliArgs) {
  const options = createOptionsFromArgs(cliArgs, defaults)
  options.debug && (process.env.DEBUG = true)

  if (options.config) {
    const userConfig = getUserConfig(options.config, defaults.paths)
    Object.assign(defaults, userConfig)
    Object.assign(options, createOptionsFromArgs(cliArgs, defaults))
    options.debug && (process.env.DEBUG = true)
  }

  const config = {
    accept: options.yes,
    debug: options.debug,
    gitInit: options.gitInit,
    gitUser: null,
    npmInstall: options.npmInstall,
    projectName: normalizeName(options.projectName),
    repository: options.repository,
    targetDir: options.targetDir,
    templateName: options.templateName,
    templates: await getTemplates(options.templatesDir, defaults.paths),
    update: options.update,
    updateAll: options.updateAll,
    verbose: options.debug ? 10 : options.verbose,
    get template () {
      return this.templates.getTemplate(this.templateName)
    }
  }

  if (config.updateAll) { return config }

  mergeConfigObjects(
    defaults,
    { templateName: config.templates.config.defaults.templateName }
  )

  if (!config.accept) {
    Object.assign(config, await getMissingOptions(config, defaults))
  }

  mergeConfigObjects(config, defaults)
  validateConfig(config)

  config.targetPath = getTargetPath(config.targetDir, config.projectName)
  config.installTree = createInstallTree(config.template, config.templates)
  config.commitTemplate = getCommitTemplate(config.installTree)
  config.commands = {
    git: createGitCommands(config.targetPath),
    npm: createNPMCommands(config.targetPath)
  }

  return config
}

module.exports = exports = createConfig
