'use strict'

function createYargsConfig (defaults) {
  return {
    options: {
      c: {
        alias: ['config'],
        description: 'Path to override config.js[on]',
        type: 'string'
      },
      /**
       * -d causes npm init to display debug output
       */
      D: {
        alias: ['debug'],
        default: defaults.debug,
        hidden: true,
        type: 'boolean'
      },
      'git-init': {
        default: defaults.gitInit,
        description: 'Run git init',
        type: 'boolean'
      },
      'npm-install': {
        default: defaults.npmInstall,
        description: 'Run npm install',
        type: 'boolean'
      },
      r: {
        alias: ['repository'],
        default: defaults.repository,
        defaultDescription: 'none',
        description: 'Git remote repository URL',
        type: 'string'
      },
      t: {
        alias: ['target-dir'],
        default: defaults.targetDir,
        defaultDescription: ' CWD',
        description: 'Project\'s target directory',
        type: 'string'
      },
      'templates-dir': {
        default: defaults.templatesDir,
        defaultDescription: 'none',
        description: 'Templates directory',
        type: 'string'
      },
      u: {
        alias: ['update'],
        default: defaults.update,
        description: 'Update pkg before npm install',
        type: 'boolean'
      },
      U: {
        alias: ['update-all'],
        default: defaults.updateAll,
        description: 'Update pkg in all templates then exit',
        type: 'boolean'
      },
      /**
       * -v causes npm init to display npm version and exit.
       */
      V: {
        alias: ['verbose'],
        description: 'Display verbose output',
        type: 'count'
      },
      y: {
        alias: ['yes'],
        default: defaults.yes,
        description: 'Accept defaults',
        type: 'boolean'
      }
    },

    positionals: {
      projectName: {
        defaultDescription: ' CWD',
        description: 'Name of new project',
        nargs: 1,
        requiresArg: true,
        type: 'string'
      },
      templateName: {
        defaultDescription: 'Menu',
        description: 'Which template to install',
        nargs: 1,
        requiresArg: true,
        type: 'string'
      }
    },

    usage: {
      command: '$0 [template-name [project-name]] [options]',
      description: 'Create a new NodeJS project using templates'
    }
  }
}

module.exports = exports = createYargsConfig
