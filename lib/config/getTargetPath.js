'use strict'

const { basename, join } = require('path')

const { getAbsolutePath } = require('../utils/fs')

function getTargetPath (targetDir, projectName) {
  return getAbsolutePath(
    basename(targetDir) === projectName
      ? targetDir
      : join(targetDir, projectName)
  )
}

module.exports = exports = getTargetPath
