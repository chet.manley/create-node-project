'use strict'

const mockEmail = 'mock-email@domain.tld'
const mockMention = 'mock-creds.username @mock.username'
const mockName = 'Mock User Name'

const mockGitCommands = {
  addAll: jest.fn(async _ => ''),
  addOrigin: jest.fn(async _ => ''),
  checkout: {
    branch: jest.fn(async _ => '')
  },
  commit: jest.fn(async _ => ''),
  config: {
    get: jest.fn(async query => query.includes('email') ? mockEmail : mockName),
    getRegexp: jest.fn(async _ => mockMention),
    set: jest.fn(async _ => '')
  },
  init: jest.fn(async _ => ''),
  push: {
    branch: jest.fn(async _ => '')
  }
}

const mockCreateGitCommands = jest.fn(_ => mockGitCommands)

module.exports = exports = mockCreateGitCommands
