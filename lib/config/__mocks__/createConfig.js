'use strict'

const createConfig = jest.fn(async cliArgs => {
  const config = {
    debug: false,
    fail: false,
    reject: false
  }

  if (cliArgs.includes('--debug')) { process.env.DEBUG = config.debug = true }
  if (cliArgs.includes('--fail')) { throw new Error('config ERROR') }
  if (cliArgs.includes('--fail-run')) { config.fail = true }
  if (cliArgs.includes('--reject-run')) { config.reject = true }

  return config
})

module.exports = exports = createConfig
