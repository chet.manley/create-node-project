'use strict'

const Template = {
  baseName: expect.any(String),
  config: expect.any(Object),
  filesPath: expect.any(String),
  manifestFile: expect.any(String),
  name: expect.any(String),
  path: expect.any(String)
}

const mockConfig = {
  accept: expect.any(Boolean),
  checkoutBranch: expect.any(String),
  commands: {
    git: {
      addAll: expect.any(Function),
      addOrigin: expect.any(Function),
      checkout: {
        branch: expect.any(Function)
      },
      commit: expect.any(Function),
      config: {
        get: expect.any(Function),
        getRegexp: expect.any(Function),
        set: expect.any(Function)
      },
      init: expect.any(Function),
      push: {
        branch: expect.any(Function)
      }
    },
    npm: {
      auditFix: expect.any(Function),
      init: expect.any(Function),
      install: expect.any(Function)
    }
  },
  commitMessage: expect.any(String),
  commitTemplate: expect.any(String),
  debug: expect.any(Boolean),
  gitInit: expect.any(Boolean),
  gitUser: null,
  installTree: expect.arrayContaining([Template]),
  npmInstall: expect.any(Boolean),
  paths: expect.arrayContaining([expect.any(String)]),
  projectName: expect.any(String),
  repository: expect.any(String),
  targetDir: expect.any(String),
  targetPath: expect.any(String),
  template: Template,
  templateName: expect.any(String),
  templates: {
    available: expect.arrayContaining([expect.any(String)]),
    config: {
      defaults: {
        manifestFile: expect.any(String),
        templateName: expect.any(String),
        templateFilesDir: expect.any(String)
      }
    },
    getTemplate: expect.any(Function),
    list: expect.arrayContaining([Template]),
    path: expect.any(String),
    templateDirs: expect.arrayContaining([expect.any(String)])
  },
  templatesDir: expect.any(String),
  update: expect.any(Boolean),
  updateAll: expect.any(Boolean),
  verbose: expect.any(Number),
  yes: expect.any(Boolean)
}

module.exports = exports = mockConfig
