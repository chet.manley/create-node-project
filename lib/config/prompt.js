'use strict'

const inquirer = require('inquirer')

function exitMenu (_, key) {
  if (key && key.name === 'escape') {
    process.exit(0)
  }
}

async function prompt (questions) {
  if (questions.length === 0) { return {} }

  process.stdin.on('keypress', exitMenu)
  console.log('ESC or ctrl-C to exit menu')

  return await inquirer.prompt(questions)
    .catch(console.error)
    .finally(_ => process.stdin.removeListener('keypress', exitMenu))
}

module.exports = exports = prompt
