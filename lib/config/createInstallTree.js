'use strict'

function createInstallTree (template, templates) {
  const installTree = [template.name]
  let nextDependencyName = template.config.dependencyName
  let dependency = null

  while (nextDependencyName) {
    dependency = templates.getTemplate(nextDependencyName)

    if (!dependency) {
      throw new Error(`'${
        installTree.slice(-1)
      }' missing dependency '${nextDependencyName}'`)
    }

    if (installTree.includes(dependency.name)) {
      throw new Error(`'${
        installTree.slice(-1)
      }' circular dependency [ ${
        installTree.slice(
          installTree.indexOf(dependency.name)
        ).join(' → ')
      } ↺ ${dependency.name} ]`)
    }

    installTree.push(dependency.name)
    nextDependencyName = dependency.config.dependencyName
  }

  return installTree.map(name => templates.getTemplate(name)).reverse()
}

module.exports = exports = createInstallTree
