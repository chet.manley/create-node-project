'use strict'

const normalizeName = require('./normalizeName')
const prompt = require('./prompt')

async function getMissingOptions (options, defaults) {
  const questions = []

  if (options.template === null) {
    questions.push({
      choices: options.templates.available,
      default: defaults.templateName,
      message: 'Choose a template :',
      name: 'templateName',
      type: 'list'
    })
  }

  if (!options.projectName) {
    questions.push({
      default: defaults.projectName,
      filter: normalizeName,
      message: 'Enter project name:',
      name: 'projectName',
      type: 'input',
      validate: response => response ? true : 'Project name cannot be empty'
    })
  }

  if (options.gitInit && !options.repository) {
    questions.push({
      message: 'Enter Git repo URL:',
      name: 'repository',
      type: 'input'
    })
  }

  return await prompt(questions)
}

module.exports = exports = getMissingOptions
