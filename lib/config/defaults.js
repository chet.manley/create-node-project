'use strict'

const { basename } = require('path')

const defaults = {
  checkoutBranch: 'integration',
  commitMessage: 'chore: initial commit :feelsgood:',
  debug: false,
  gitInit: true,
  npmInstall: true,
  paths: [
    '~/',
    '~/.create-node-project',
    '~/.config',
    '~/.config/create-node-project'
  ],
  projectName: basename(process.cwd()),
  repository: '',
  targetDir: process.cwd(),
  templatesDir: '',
  update: false,
  updateAll: false,
  yes: false
}

module.exports = exports = defaults
