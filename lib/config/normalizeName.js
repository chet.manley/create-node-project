'use strict'

const normalizeName = (name = '') =>
  name.toLowerCase().trim().replace(/[ ]/g, '-')

module.exports = exports = normalizeName
