'use strict'

const { createConfig } = require('./config')
const { getTasks } = require('./tasks')

async function cli (args) {
  process.exitCode = 0

  try {
    const config = await createConfig(args)

    await getTasks(config).run()
  } catch (error) {
    process.exitCode = error.exitCode || 1
    process.env.DEBUG && console.debug('DEBUG', error)
    process.env.DEBUG || console.error(`Critical Error: ${error.message}`)
  }

  return process.exitCode
}

module.exports = exports = cli
