'use strict'

const asyncPipe = (...fns) => async args =>
  await fns.reduce(async (a, f) => f(await a), args)

module.exports = exports = asyncPipe
