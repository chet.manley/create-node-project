'use strict'

const utils = {
  fs: require('./fs'),

  asyncPipe: require('./asyncPipe'),
  createDictionary: require('./createDictionary'),
  createFindAndReplace: require('./createFindAndReplace'),
  createShellCommand: require('./createShellCommand'),
  getStringFromMatches: require('./getStringFromMatches'),
  getValueFromObjectPath: require('./getValueFromObjectPath'),
  inspect: require('./inspect'),
  updateDependencies: require('./updateDependencies')
}

module.exports = exports = utils
