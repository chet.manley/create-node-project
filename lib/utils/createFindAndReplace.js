'use strict'

function createFindAndReplace (pattern, dictionary) {
  function findAndReplace (content) {
    return content.replace(pattern, (match, placeholder) =>
      dictionary._find(placeholder) || match
    )
  }
  findAndReplace.constructor = createFindAndReplace

  return findAndReplace
}
createFindAndReplace.of = createFindAndReplace

module.exports = exports = createFindAndReplace
