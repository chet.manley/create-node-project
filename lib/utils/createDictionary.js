'use strict'

const getValueFromObjectPath = require('./getValueFromObjectPath')

function createDictionary (obj) {
  obj._find = path => getValueFromObjectPath(obj, path)
  obj.constructor = createDictionary

  return obj
}
createDictionary.of = createDictionary

module.exports = exports = createDictionary
