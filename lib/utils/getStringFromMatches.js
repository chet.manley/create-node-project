'use strict'

function getStringFromMatches (...matches) {
  return matches
    .filter(val => val && val[0])
    .map(val => val[0].trim())
    .join('\n')
}

module.exports = exports = getStringFromMatches
