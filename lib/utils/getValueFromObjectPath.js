'use strict'

function getValueFromObjectPath (object, path) {
  return path
    .split('.')
    .reduce((o, k) => o && o[k], object)
}

module.exports = exports = getValueFromObjectPath
