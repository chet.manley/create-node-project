'use strict'

const { copyFile, readdir } = require('fs').promises
const { join } = require('path')

const createDirRecursive = require('./createDirRecursive')
const isDirectory = require('./isDirectory')

async function copyRecursive (source, target) {
  if (await isDirectory(source)) {
    await createDirRecursive(target)

    for (const file of await readdir(source)) {
      await copyRecursive(join(source, file), join(target, file))
    }
  } else {
    await copyFile(source, target)
  }

  return true
}

module.exports = exports = copyRecursive
