'use strict'

const { mkdir } = require('fs').promises

const createDirRecursive = absPath => mkdir(absPath, { recursive: true })

module.exports = exports = createDirRecursive
