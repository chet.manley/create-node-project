'use strict'

const { readFile } = require('fs').promises

const readTextFile = async absPath => (await readFile(absPath)).toString()

module.exports = exports = readTextFile
