'use strict'

const fs = {
  calculatePaths: require('./calculatePaths'),
  copyRecursive: require('./copyRecursive'),
  createDirRecursive: require('./createDirRecursive'),
  enumerateDirectory: require('./enumerateDirectory'),
  getAbsolutePath: require('./getAbsolutePath'),
  isDirectory: require('./isDirectory'),
  isEmptyDirectory: require('./isEmptyDirectory'),
  parseJsonFile: require('./parseJsonFile'),
  readTextFile: require('./readTextFile')
}

module.exports = exports = fs
