'use strict'

const { readdir } = require('fs').promises

const enumerateDirectory = async absPath => await readdir(absPath)

module.exports = exports = enumerateDirectory
