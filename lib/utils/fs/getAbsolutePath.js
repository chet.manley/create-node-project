'use strict'

const { homedir } = require('os')
const { isAbsolute, join } = require('path')

function getAbsolutePath (subPath) {
  const path = subPath.replace('~', homedir)

  return isAbsolute(path) ? path : join(process.cwd(), path)
}

module.exports = exports = getAbsolutePath
