'use strict'

const { isAbsolute, join } = require('path')

const getAbsolutePath = require('./getAbsolutePath')

function calculatePaths (file = '', defaultPaths = []) {
  const paths = isAbsolute(file)
    ? [file]
    : [
        getAbsolutePath(file),
        ...defaultPaths.map(path => join(getAbsolutePath(path), file))
      ]

  return paths
}

module.exports = exports = calculatePaths
