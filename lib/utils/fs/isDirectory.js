'use strict'

const { lstat } = require('fs').promises

const isDirectory = async path => (await lstat(path)).isDirectory()

module.exports = exports = isDirectory
