'use strict'

const { homedir } = require('os')
const { join } = require('path')

const calculatePaths = require('../calculatePaths.js')

const mockFileName = 'filename.ext'
const mockFileAbsPath = '/mock/filename.ext'
const mockFileHomePath = `~/${mockFileName}`
const mockFileRelPath = 'mock/filename.ext'
const mockPaths = [
  '~/',
  '/tmp/mock'
]

describe('calculatePaths Function', () => {
  beforeEach(() => jest.clearAllMocks())

  describe('returns an Array', () => {
    it('should return CWD if passed no arguments', () => {
      expect(calculatePaths()).toEqual([process.cwd()])
    })

    it('should return input on absolute file path', () => {
      expect(calculatePaths(mockFileAbsPath)).toEqual([mockFileAbsPath])
      expect(calculatePaths(mockFileAbsPath, mockPaths))
        .toEqual([mockFileAbsPath])
    })

    it('should join CWD+input on relative file path', () => {
      expect(calculatePaths(mockFileName))
        .toEqual([join(process.cwd(), mockFileName)])
      expect(calculatePaths(mockFileRelPath))
        .toEqual([join(process.cwd(), mockFileRelPath)])
    })

    it('should resolve file name with default paths', () => {
      const expected = [
        join(process.cwd(), mockFileName),
        join(homedir(), mockFileName),
        join(mockPaths[1], mockFileName)
      ]

      expect(calculatePaths(mockFileName, mockPaths)).toEqual(expected)
    })

    it('should resolve relative file path with default paths', () => {
      const expected = [
        join(process.cwd(), mockFileRelPath),
        join(homedir(), mockFileRelPath),
        join(mockPaths[1], mockFileRelPath)
      ]

      expect(calculatePaths(mockFileRelPath, mockPaths)).toEqual(expected)
    })

    it('should convert "~/" in file path to absolute home directory', () => {
      expect(calculatePaths(mockFileHomePath))
        .toEqual([join(homedir(), mockFileName)])
    })
  })
})
