'use strict'

const { vol } = require('memfs')

const parseJsonFile = require('../parseJsonFile.js')

const jsonArray = ['val1', 'val2']
const jsonObject = { key: 'value' }
const jsonString = 'string'
const jsonInvalid = '...'
const jsonArrayFile = '/tmp/array.json'
const jsonObjectFile = '/tmp/object.json'
const jsonStringFile = '/tmp/string.json'
const jsonInvalidFile = '/tmp/invalid.json'
const jsonMissingFile = '/tmp/missing.json'
const mockVolume = {
  '/tmp/array.json': JSON.stringify(jsonArray),
  '/tmp/object.json': JSON.stringify(jsonObject),
  '/tmp/string.json': JSON.stringify(jsonString),
  '/tmp/invalid.json': jsonInvalid
}

describe('parseJsonFile AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    vol.fromJSON(mockVolume)
  })

  afterEach(() => vol.reset())

  it('should reject with Error if missing argument', async () => {
    await expect(parseJsonFile()).rejects.toThrow(TypeError)
  })

  it('should reject with ENOENT Error if file is missing', async () => {
    await expect(parseJsonFile(jsonMissingFile)).rejects.toThrow(Error)
  })

  it(
    'should reject with SyntaxError if file does not contain JSON',
    async () => {
      await expect(parseJsonFile(jsonInvalidFile)).rejects.toThrow(SyntaxError)
    })

  it('should resolve to Array if file contains JSON array', async () => {
    await expect(parseJsonFile(jsonArrayFile)).resolves.toEqual(jsonArray)
  })

  it('should resolve to Object if file contains JSON object', async () => {
    await expect(parseJsonFile(jsonObjectFile)).resolves.toEqual(jsonObject)
  })

  it('should resolve to String if file contains JSON string', async () => {
    await expect(parseJsonFile(jsonStringFile)).resolves.toEqual(jsonString)
  })
})
