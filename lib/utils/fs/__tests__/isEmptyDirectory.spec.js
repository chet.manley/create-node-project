'use strict'

const { vol } = require('memfs')

const isEmptyDirectory = require('../isEmptyDirectory.js')

const mockEmptyDirectory = '/tmp/mock'
const mockNonEmptyDirectory = '/tmp/mock2'
const mockVolume = {
  '/tmp/mock': null,
  '/tmp/mock2/file.ext': '...'
}
const mockFile = '/tmp/file.ext'
const mockMissingFile = '/tmp/bad'

describe('isEmptyDirectory AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    vol.fromJSON(mockVolume)
  })

  afterEach(() => vol.reset())

  it('should reject with Error if missing argument', async () => {
    await expect(isEmptyDirectory()).rejects.toThrow(TypeError)
  })

  it('should reject with ENOENT Error if file is missing', async () => {
    vol.reset()

    await expect(isEmptyDirectory(mockMissingFile)).rejects.toThrow()
  })

  it(
    'should reject with ENOTDIR Error if file is not a directory',
    async () => {
      await expect(isEmptyDirectory(mockFile)).rejects.toThrow()
    })

  it('should resolve to true if directory is empty', async () => {
    await expect(isEmptyDirectory(mockEmptyDirectory)).resolves.toBe(true)
  })

  it('should resolve to false if directory is not empty', async () => {
    await expect(isEmptyDirectory(mockNonEmptyDirectory)).resolves.toBe(false)
  })
})
