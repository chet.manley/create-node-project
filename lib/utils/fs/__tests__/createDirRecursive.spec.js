'use strict'

const { vol } = require('memfs')

const createDirRecursive = require('../createDirRecursive.js')

const mockDirectories = {
  '/tmp/mock1': {
    return: '/tmp/mock1',
    vol: '/tmp/mock1'
  },
  '/tmp/mock1/': {
    return: undefined,
    vol: '/tmp/mock1'
  },
  '/tmp/mock2/abs': {
    return: '/tmp/mock2',
    vol: '/tmp/mock2/abs'
  },
  '/tmp/mock3/abs2/': {
    return: '/tmp/mock3',
    vol: '/tmp/mock3/abs2'
  },
  './mock4/.rel': {
    return: `${process.cwd()}/mock4`,
    vol: `${process.cwd()}/mock4/.rel`
  },
  'mock5/rel': {
    return: `${process.cwd()}/mock5`,
    vol: `${process.cwd()}/mock5/rel`
  }
}

describe('createDirRecursive AsyncFunction', () => {
  beforeEach(() => jest.clearAllMocks())

  afterEach(() => vol.reset())

  it('should reject with Error if missing argument', async () => {
    await expect(createDirRecursive()).rejects.toThrow(TypeError)
  })

  it('should resolve to first directory path created', async () => {
    for (const [dir, meta] of Object.entries(mockDirectories)) {
      /**
        * Blocking: memfs.mkdir always returns void
        * https://github.com/streamich/memfs/issues/546
        * await expect(createDirRecursive(dir)).resolves.toBe(meta.return)
        */
      await expect(createDirRecursive(dir)).resolves.toBeUndefined()
      expect(vol.toJSON()).toHaveProperty([meta.vol], null)
    }

    expect.assertions(mockDirectories.length)
  })
})
