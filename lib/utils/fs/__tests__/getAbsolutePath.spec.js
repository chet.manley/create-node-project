'use strict'

const { homedir } = require('os')
const { isAbsolute, join } = require('path')

const getAbsolutePath = require('../getAbsolutePath.js')

const mockFileName = 'filename.ext'
const mockFileAbsPath = '/mock/filename.ext'
const mockFileHomePath = `~/${mockFileName}`
const mockFileRelPath = 'mock/filename.ext'

describe('getAbsolutePath Function', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should throw TypeError if no missing argument', () => {
    expect(_ => getAbsolutePath()).toThrow(TypeError)
  })

  it('should return input on absolute path', () => {
    expect(getAbsolutePath(mockFileAbsPath)).toEqual(mockFileAbsPath)
  })

  it('should join CWD with relative paths', () => {
    const res1 = getAbsolutePath(mockFileName)
    const res2 = getAbsolutePath(mockFileRelPath)

    expect(isAbsolute(res1)).toBe(true)
    expect(res1).toEqual(join(process.cwd(), mockFileName))
    expect(isAbsolute(res2)).toBe(true)
    expect(res2).toEqual(join(process.cwd(), mockFileRelPath))
  })

  it('should resolve "~/" to home directory', () => {
    const res = getAbsolutePath(mockFileHomePath)

    expect(isAbsolute(res)).toBe(true)
    expect(res).toEqual(join(homedir(), mockFileName))
  })
})
