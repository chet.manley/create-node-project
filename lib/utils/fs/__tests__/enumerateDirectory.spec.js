'use strict'

const { vol } = require('memfs')

const enumerateDirectory = require('../enumerateDirectory.js')

const mockBadDirectory = '/tmp/bad'
const mockDirectory = '/tmp/mock'
const mockVolume = {
  'file.ext': '...',
  '.hidden.ext': '...',
  'subdir/file.ext': '...',
  'empty-dir': null,
  '.hidden-dir': null
}
const enumerated = [
  '.hidden-dir',
  '.hidden.ext',
  'empty-dir',
  'file.ext',
  'subdir'
]

describe('enumerateDirectory AsyncFunction', () => {
  beforeEach(() => jest.clearAllMocks())

  afterEach(() => vol.reset())

  it('should reject with Error if missing argument', async () => {
    await expect(enumerateDirectory()).rejects.toThrow(TypeError)
  })

  it('should reject with ENOENT Error if directory is missing', async () => {
    await expect(enumerateDirectory(mockBadDirectory))
      .rejects.toThrow()
  })

  it('should resolve to Array of files', async () => {
    vol.fromJSON(mockVolume, mockDirectory)

    const res = await enumerateDirectory(mockDirectory)

    expect(res).toHaveLength(Object.keys(mockVolume).length)
    expect(res).toEqual(enumerated)
  })
})
