'use strict'

const { vol } = require('memfs')

const isDirectory = require('../isDirectory.js')

const mockDirectory = '/tmp/mock'
const mockVolume = {
  '/tmp/mock': null,
  '/tmp/file.ext': '...'
}
const mockFile = '/tmp/file.ext'
const mockMissingFile = '/tmp/bad'

describe('isDirectory AsyncFunction', () => {
  beforeEach(() => jest.clearAllMocks())

  afterEach(() => vol.reset())

  it('should reject with Error if missing argument', async () => {
    await expect(isDirectory()).rejects.toThrow(TypeError)
  })

  it('should reject with ENOENT Error if file is missing', async () => {
    await expect(isDirectory(mockMissingFile)).rejects.toThrow()
  })

  it('should resolve to true if file is a directory', async () => {
    vol.fromJSON(mockVolume)

    await expect(isDirectory(mockDirectory)).resolves.toBe(true)
  })

  it('should resolve to false if file is not a directory', async () => {
    vol.fromJSON(mockVolume)

    await expect(isDirectory(mockFile)).resolves.toBe(false)
  })
})
