'use strict'

const { vol } = require('memfs')

const copyRecursive = require('../copyRecursive.js')

const mockDestination = '/tmp/mock-cp'
const mockSource = '/tmp/mock'

const mockDirectory = {
  './file1.ext': '...',
  './file2.ext': '...',
  './dir2/file1.ext': '...',
  './empty-dir': null
}
const mockFile = `${mockSource}/dir2/file1.ext`
const mockFileCopy = '/tmp/file1.ext'

const preOpVol = {
  '/tmp/mock/file1.ext': '...',
  '/tmp/mock/file2.ext': '...',
  '/tmp/mock/dir2/file1.ext': '...',
  '/tmp/mock/empty-dir': null
}
const postOpVol = {
  '/tmp/mock/file1.ext': '...',
  '/tmp/mock/file2.ext': '...',
  '/tmp/mock/dir2/file1.ext': '...',
  '/tmp/mock/empty-dir': null,
  '/tmp/mock-cp/dir2/file1.ext': '...',
  '/tmp/mock-cp/empty-dir': null,
  '/tmp/mock-cp/file1.ext': '...',
  '/tmp/mock-cp/file2.ext': '...'
}

describe('copyRecursive AsyncFunction', () => {
  beforeEach(() => {
    vol.fromJSON(mockDirectory, mockSource)
    jest.clearAllMocks()
  })

  afterEach(() => vol.reset())

  it('should reject with Error if missing arguments', async () => {
    await expect(copyRecursive()).rejects.toThrow(TypeError)
    await expect(copyRecursive(mockSource)).rejects.toThrow(TypeError)
    await expect(copyRecursive(undefined, mockDestination))
      .rejects.toThrow(TypeError)
  })

  it('should resolve to true on success', async () => {
    expect(vol.toJSON()).toEqual(preOpVol)
    await expect(copyRecursive(mockSource, mockDestination))
      .resolves.toBe(true)
    expect(vol.toJSON()).toEqual(postOpVol)
  })

  it('should copy single files', async () => {
    const expectedVol = { ...vol.toJSON() }
    expectedVol[mockFileCopy] = expectedVol[mockFile]

    expect(vol.toJSON()).toEqual(preOpVol)
    await expect(copyRecursive(mockFile, mockFileCopy)).resolves.toBe(true)
    expect(vol.toJSON()).toEqual(expectedVol)
  })

  it('should rename single files', async () => {
    const expectedVol = { ...vol.toJSON() }
    expectedVol[mockDestination] = expectedVol[mockFile]

    expect(vol.toJSON()).toEqual(preOpVol)
    await expect(copyRecursive(mockFile, mockDestination)).resolves.toBe(true)
    expect(vol.toJSON()).toEqual(expectedVol)
  })
})
