'use strict'

const { vol } = require('memfs')

const readTextFile = require('../readTextFile.js')

const mockFile = '/tmp/file.ext'
const mockDirFile = '/tmp'
const mockMissingFile = '/tmp/missing.json'
const mockFileContents = 'mock string'
const mockVolume = {
  '/tmp/file.ext': mockFileContents
}

describe('readTextFile AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    vol.fromJSON(mockVolume)
  })

  afterEach(() => vol.reset())

  it('should reject with Error if missing argument', async () => {
    await expect(readTextFile()).rejects.toThrow(TypeError)
  })

  it('should reject with ENOENT Error if file is missing', async () => {
    await expect(readTextFile(mockMissingFile)).rejects.toThrow()
  })

  it('should reject with EISDIR Error if file is a directory', async () => {
    await expect(readTextFile(mockDirFile)).rejects.toThrow()
  })

  it('should resolve to file contents', async () => {
    await expect(readTextFile(mockFile)).resolves.toEqual(mockFileContents)
  })
})
