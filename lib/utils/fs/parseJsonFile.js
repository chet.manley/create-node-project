'use strict'

const readTextFile = require('./readTextFile')

const parseJsonFile = async absPath => JSON.parse(await readTextFile(absPath))

module.exports = exports = parseJsonFile
