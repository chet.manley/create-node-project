'use strict'

const { readdir } = require('fs').promises

const isEmptyDirectory = async path => (await readdir(path)).length === 0

module.exports = exports = isEmptyDirectory
