'use strict'

const { spawn } = require('child_process')

const createShellCommand = function (
  command,
  cwd = process.cwd()
) {
  function setup (...setupArgs) {
    const options = {
      cwd,
      encoding: 'utf8'
    }

    return function run (...args) {
      const child = spawn(command, [...setupArgs, ...args], options)
      const promise = new Promise((resolve, reject) => {
        let res = ''

        child.stdout.on('data', chunk => { res += chunk })
        child.on('error', reject)
        child.on(
          'exit',
          code => code === 0
            ? resolve(res.trim())
            : reject(new Error(`Exit code ${code}`))
        )
      })
      promise.stdin = child.stdin
      promise.stdout = child.stdout
      promise.stderr = child.stderr

      return promise
    }
  }
  setup.constructor = createShellCommand

  return setup
}
createShellCommand.of = createShellCommand

module.exports = exports = createShellCommand
