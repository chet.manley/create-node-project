'use strict'

const { join } = require('path')

const ncu = require('npm-check-updates')

async function updateDependencies (
  projectPath,
  {
    checkOnly = false,
    manifestFile = 'package.json'
  } = {}
) {
  const packageFile = join(projectPath, manifestFile)

  try {
    const updates = await ncu.run({
      packageFile,
      silent: true,
      upgrade: !checkOnly
    })

    return `Applied ${Object.keys(updates).length} updates`
  } catch (error) {
    if (error.code === 'EACCES') {
      throw new Error(`Permission denied updating ${manifestFile}`)
    }
    throw error
  }
}

module.exports = exports = updateDependencies
