'use strict'

const { basename } = require('path')

const mockNcu = require('npm-check-updates')

const updateDependencies = require('../updateDependencies.js')

const defaultProjectManifest = 'package.json'
const mockOptions = {
  manifestFile: 'pkg.json',
  checkOnly: true
}
const mockProjectPath = '/test/project'

describe('updateDependencies AsyncFunction', () => {
  beforeEach(() => {
    mockNcu.__setError(null)
    mockNcu.__setResults()
    jest.clearAllMocks()
  })

  it('should reject with Error if missing arguments', async () => {
    await expect(updateDependencies()).rejects.toThrow()
  })

  it('should reject with custom Error on EACCES error', async () => {
    const error = new Error()
    error.code = 'EACCES'
    mockNcu.__setError(error)

    await expect(updateDependencies(mockProjectPath))
      .rejects.toThrow('Permission denied updating package.json')
  })

  it('should reject with Error on failure', async () => {
    const error = new Error('mock error')
    mockNcu.__setError(error)

    await expect(updateDependencies(mockProjectPath))
      .rejects.toThrow(error)
  })

  it('should resolve to String on success', async () => {
    mockNcu.__setResults(3)

    await expect(updateDependencies(mockProjectPath))
      .resolves.toBe('Applied 3 updates')
  })

  it('should default `checkOnly` option to false', async () => {
    await expect(updateDependencies(mockProjectPath))
      .resolves.toBeType('string')
    expect(mockNcu.run).toHaveBeenCalledTimes(1)
    expect(mockNcu.run).toHaveBeenLastCalledWith(
      expect.objectContaining({ upgrade: true })
    )
  })

  it('should override `checkOnly` from options argument', async () => {
    await expect(updateDependencies(mockProjectPath, mockOptions))
      .resolves.toBeType('string')
    expect(mockNcu.run).toHaveBeenCalledTimes(1)
    expect(mockNcu.run).toHaveBeenLastCalledWith(
      expect.objectContaining({ upgrade: false })
    )
  })

  it('should default `projectManifest` option to "package.json"', async () => {
    await expect(updateDependencies(mockProjectPath))
      .resolves.toBeType('string')
    expect(mockNcu.run).toHaveBeenCalledTimes(1)
    expect(basename(mockNcu.run.mock.calls[0][0].packageFile))
      .toEqual(defaultProjectManifest)
  })

  it('should override `projectManifest` from options argument', async () => {
    await expect(updateDependencies(mockProjectPath, mockOptions))
      .resolves.toBeType('string')
    expect(mockNcu.run).toHaveBeenCalledTimes(1)
    expect(basename(mockNcu.run.mock.calls[0][0].packageFile))
      .toEqual(mockOptions.manifestFile)
  })
})
