'use strict'

const createDictionary = require('../createDictionary.js')

const mockDictionary = {
  key1: {
    key2: 'value2'
  }
}
const dictionary = createDictionary(mockDictionary)

describe('createDictionary Factory', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should return a dictionary Object', () => {
    expect(dictionary).toBeType('object')
    expect(dictionary.constructor).toBe(createDictionary)
  })

  describe('dictionary Object', () => {
    it('should contain all properties of object passed to factory', () => {
      expect(dictionary).toMatchObject(mockDictionary)
    })

    it('should provide _find method', () => {
      expect(dictionary).toHaveProperty('_find', expect.any(Function))
    })

    describe('#_find() method', () => {
      it('should return value of matched key paths', () => {
        expect(dictionary._find('key1')).toEqual(mockDictionary.key1)
        expect(dictionary._find('key1.key2')).toEqual(mockDictionary.key1.key2)
      })

      it('should return `undefined` for unmatched key path', () => {
        expect(dictionary._find('key3')).toBeUndefined()
      })
    })
  })
})
