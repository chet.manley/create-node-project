'use strict'

const createDictionary = require('../createDictionary.js')
const createFindAndReplace = require('../createFindAndReplace.js')

const mockDictionary = createDictionary({
  key1: {
    key2: 'value2'
  },
  key3: 'value3'
})
const mockPattern = /{{ ([\w.]+) }}/g

const mockContent = 'mock {{ key1.key2 }} mock'
const mockContentResult = 'mock value2 mock'
const mockMultilineContent =
`
mock {{ key1.key2 }} mock,
mock {{ key3 }} fullstop.
`
const mockMultilineContentResult =
`
mock value2 mock,
mock value3 fullstop.
`

const far = createFindAndReplace(mockPattern, mockDictionary)

/**
 * These tests cover how the module SHOULD be used.
 * Specifically, implementing a test pattern with the global flag.
 * Testing edge cases outside of these parameters is not
 * necessary for this project's use case.
 */

describe('createFindAndReplace Factory', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should return findAndReplace Function', () => {
    expect(far).toBeInstanceOf(Function)
    expect(far.name).toEqual('findAndReplace')
    expect(far.constructor).toBe(createFindAndReplace)
  })

  describe('findAndReplace Function', () => {
    it('should replace placeholders in provided content', () => {
      expect(far(mockContent)).toEqual(mockContentResult)
    })

    it('Should replace all placeholders in multiline content', () => {
      expect(far(mockMultilineContent)).toEqual(mockMultilineContentResult)
    })

    it('should skip placeholders with no dictionary entry', () => {
      expect(far('mock {{ key1.key3 }} mock'))
        .toEqual('mock {{ key1.key3 }} mock')
    })
  })
})
