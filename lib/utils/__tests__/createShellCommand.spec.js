'use strict'

const { Readable, Writable } = require('stream')

const mockSpawn = require('child_process').spawn

const createShellCommand = require('../createShellCommand.js')

const mockCommand = 'mock-command'
const mockCWD = '/tmp/mock'
const mockSetupArguments = ['--pre']
const mockRunArguments = ['--post']

describe('createShellCommand Factory', () => {
  const setup = createShellCommand(mockCommand, mockCWD)

  beforeEach(() => jest.clearAllMocks())

  it('should return setup Function', () => {
    expect(setup).toBeInstanceOf(Function)
    expect(setup.name).toEqual('setup')
    expect(setup.constructor).toBe(createShellCommand)
  })

  describe('setup Function', () => {
    it('should return run Function', () => {
      const run = setup()

      expect(run).toBeInstanceOf(Function)
      expect(run.name).toEqual('run')
    })

    describe('run Function', () => {
      it('should spawn process with command passed from factory', () => {
        setup()()

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][0]).toEqual(mockCommand)
      })

      it('should spawn process with arguments passed from setup', () => {
        setup(...mockSetupArguments)()

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][1]).toEqual(mockSetupArguments)
      })

      it('should spawn process with arguments passed to run', () => {
        setup()(...mockRunArguments)

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][1]).toEqual(mockRunArguments)
      })

      it('should spawn process with merged arguments', () => {
        setup(...mockSetupArguments)(...mockRunArguments)

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][1]).toEqual([
          ...mockSetupArguments,
          ...mockRunArguments
        ])
      })

      it('should spawn process with CWD from factory', () => {
        setup()()

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][2].cwd).toEqual(mockCWD)
      })

      it('should spawn process with UTF8 encoding', () => {
        setup()()

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][2].encoding).toEqual('utf8')
      })

      it('should default CWD to PWD if not provided', () => {
        createShellCommand(mockCommand)()()

        expect(mockSpawn).toHaveBeenCalledTimes(1)
        expect(mockSpawn.mock.calls[0][2].cwd).toEqual(process.cwd())
      })

      it('should return custom Promise with attached stdio streams', () => {
        const res = setup()()

        expect(res).toBeInstanceOf(Promise)
        expect(res.stdin).toBeInstanceOf(Writable)
        expect(res.stdout).toBeInstanceOf(Readable)
        expect(res.stderr).toBeInstanceOf(Readable)
      })

      it('should reject with Error if process exit code not 0', async () => {
        const res = setup()()
        const msp = mockSpawn.mock.results[0].value

        msp.stdout.push('mock command stdout')
        msp.exit(1)

        await expect(res).rejects.toThrow('Exit code 1')
      })

      it('should reject with Error if process errors', async () => {
        const res = setup()()
        const msp = mockSpawn.mock.results[0].value

        msp.stdout.push('mock command stdout')
        msp.error(new Error('mock process failed'))

        await expect(res).rejects.toThrow('mock process failed')
      })

      it('should resolve with stdout if process exit code is 0', async () => {
        const res = setup()()
        const msp = mockSpawn.mock.results[0].value

        msp.stdout.push('mock command stdout')
        msp.exit()

        await expect(res).resolves.toEqual('mock command stdout')
      })
    })
  })
})
