'use strict'

const getValueFromObjectPath = require('../getValueFromObjectPath.js')

const mockObject = {
  key1: {
    key2: 'value2'
  }
}

describe('getValueFromObjectPath Function', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should throw if arguments are missing', () => {
    expect(_ => getValueFromObjectPath()).toThrow(TypeError)
    expect(_ => getValueFromObjectPath(mockObject)).toThrow(TypeError)
  })

  it('should resolve value of single-level path', () => {
    expect(getValueFromObjectPath(mockObject, 'key1'))
      .toEqual(mockObject.key1)
  })

  it('should resolve value of multi-level path', () => {
    expect(getValueFromObjectPath(mockObject, 'key1.key2'))
      .toEqual(mockObject.key1.key2)
  })

  it('should return `undefined` for missing keys in path', () => {
    expect(getValueFromObjectPath(mockObject, 'key3')).toBeUndefined()
    expect(getValueFromObjectPath(mockObject, 'key1.key3')).toBeUndefined()
    expect(getValueFromObjectPath(undefined, 'key1')).toBeUndefined()
  })
})
