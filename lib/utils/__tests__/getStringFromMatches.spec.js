'use strict'

const getStringFromMatches = require('../getStringFromMatches.js')

const mockInput = [
  null,
  [''],
  ['line  1'],
  ['line 2  '],
  [' line 3'],
  [' line 4  ']
]
const mockOutput =
`line  1
line 2
line 3
line 4`

describe('getStringFromMatches Function', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should return a String', () => {
    expect(getStringFromMatches()).toBeType('string')
  })

  it('should return empty String on empty Arrays, or `null`', () => {
    expect(getStringFromMatches([], null, undefined, [''])).toEqual('')
  })

  it('should join multiple match results into multiline String', () => {
    expect(getStringFromMatches(...mockInput)).toEqual(mockOutput)
  })

  it('should remove leading/trailing whitespace from each match', () => {
    const res = getStringFromMatches(...mockInput)
    const lines = res.split('\n')

    for (const line of lines) {
      expect(line).toMatch(/^\S+.*?\S+$/)
    }

    expect.assertions(lines.length)
  })
})
