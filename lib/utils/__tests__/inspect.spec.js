'use strict'

const inspect = require('../inspect.js')

const consoleDirSpy = jest.spyOn(console, 'dir')
  .mockImplementation(_ => {})
const defaultOptions = { showHidden: false, depth: null, colors: true }

describe('inspect Function', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should call `console.dir` with default options', () => {
    expect(inspect('test')).toBeUndefined()
    expect(consoleDirSpy).toHaveBeenCalledTimes(1)
    expect(consoleDirSpy).toHaveBeenLastCalledWith('test', defaultOptions)
  })

  it('should call `console.dir` with showHidden option', () => {
    expect(inspect('test', true)).toBeUndefined()
    expect(consoleDirSpy).toHaveBeenCalledTimes(1)
    expect(consoleDirSpy).toHaveBeenLastCalledWith(
      'test',
      expect.objectContaining({ showHidden: true })
    )
  })
})
