'use strict'

const inspect = (object, showHidden = false) =>
  console.dir(object, { showHidden, depth: null, colors: true })

module.exports = exports = inspect
