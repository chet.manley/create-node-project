'use strict'

const templates = {
  getTemplates: require('./getTemplates'),
  installTemplates: require('./installTemplates')
}

module.exports = exports = templates
