'use strict'

const { asyncPipe } = require('../utils')

const {
  copyFiles,
  createFarDictionary,
  fillPlaceholders,
  mergeManifest,
  removeFiles,
  renameFiles,
  verifyFilesPath
} = require('./install')

async function installTemplates (config) {
  const installPipe = asyncPipe(
    verifyFilesPath,
    copyFiles,
    mergeManifest,
    createFarDictionary,
    fillPlaceholders,
    renameFiles,
    removeFiles
  )

  for (const template of config.installTree) {
    const installConfig = {
      dictionary: {},
      files: {
        fill: template.config.far || [],
        filled: [],
        remove: [],
        removed: [],
        rename: template.config.rename || {},
        renamed: []
      },
      filesPath: template.filesPath,
      manifest: '',
      manifestFile: template.manifestFile,
      name: template.name
    }

    await installPipe([config, installConfig])
  }

  return config
}

module.exports = exports = installTemplates
