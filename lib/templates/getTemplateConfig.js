'use strict'

const { join } = require('path')

function getTemplateConfig (templatePath) {
  try {
    return require(join(templatePath, 'config'))
  } catch (error) {
    if (error.code === 'MODULE_NOT_FOUND') { return {} }
    if (error.code === 'EACCES') {
      throw new Error('Permission denied reading template config')
    }
    throw error
  }
}

module.exports = exports = getTemplateConfig
