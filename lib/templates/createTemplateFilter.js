'use strict'

const { join } = require('path')

const {
  isDirectory,
  isEmptyDirectory
} = require('../utils/fs')

const templatePredicate = async absPath =>
  await isDirectory(absPath) && !await isEmptyDirectory(absPath)

function createTemplateFilter (templatesPath) {
  return async function filter (files) {
    const templateDirs = []

    for (const file of files) {
      if (await templatePredicate(join(templatesPath, file))) {
        templateDirs.push(file)
      }
    }

    return templateDirs
  }
}
createTemplateFilter.of = createTemplateFilter

module.exports = exports = createTemplateFilter
