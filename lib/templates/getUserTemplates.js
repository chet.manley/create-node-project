'use strict'

const {
  calculatePaths,
  enumerateDirectory
} = require('../utils/fs')

const createTemplateFilter = require('./createTemplateFilter')
const getTemplateConfig = require('./getTemplateConfig')

async function getUserTemplates (templatesDir, defaultPaths) {
  const paths = calculatePaths(templatesDir, defaultPaths)

  for (const path of paths) {
    try {
      try {
        const templates = require(path)

        if (templates) {
          if (
            Array.isArray(templates.templateDirs) &&
            typeof templates.path === 'string' &&
            templates.config instanceof Object &&
            templates.config.constructor === Object
          ) { return templates }

          continue
        }
      } catch (error) {
        if (error.code !== 'MODULE_NOT_FOUND') { throw error }
      }

      const list = await enumerateDirectory(path)
      const templateDirs = await createTemplateFilter(path)(list)
      const config = getTemplateConfig(path)

      return {
        config,
        path,
        templateDirs
      }
    } catch (error) {
      if (error.code === 'EACCES') {
        throw new Error('Permission denied accessing user templates')
      }
      if (error.code !== 'ENOENT') { throw error }
    }
  }

  throw new Error('User templates directory not found')
}

module.exports = exports = getUserTemplates
