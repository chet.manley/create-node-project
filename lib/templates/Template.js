'use strict'

const { basename, join } = require('path')

const { updateDependencies } = require('../utils')

const getTemplateConfig = require('./getTemplateConfig')

class Template {
  constructor (templatePath, defaults) {
    this.path = templatePath
    this.baseName = basename(this.path)
    this.config = getTemplateConfig(this.path)
    this.filesPath = join(
      this.path,
      this.config.filesDir || defaults.templateFilesDir
    )
    this.manifestFile = this.config.manifestFile || defaults.manifestFile
    this.name = this.config.name || this.baseName

    return this
  }

  async update () {
    return await updateDependencies(
      this.filesPath,
      { manifestFile: this.manifestFile, checkOnly: true }
    )
  }
}

module.exports = exports = Template
