'use strict'

const { join } = require('path')

const Template = require('./Template')
const getUserTemplates = require('./getUserTemplates')
const templates = require('./templates')

async function getTemplates (templatesDir, defaultPaths) {
  if (templates.list.length !== 0) { return templates }

  Object.assign(
    templates,
    templatesDir
      ? await getUserTemplates(templatesDir, defaultPaths)
      : require('@chet.manley/node-project-templates')
  )

  if (templates.templateDirs.length === 0) {
    templates.list = []
    throw new Error('No templates found')
  }

  for (const templateDir of templates.templateDirs) {
    templates.list.push(new Template(
      join(templates.path, templateDir),
      templates.config.defaults
    ))
  }

  return templates
}

module.exports = exports = getTemplates
