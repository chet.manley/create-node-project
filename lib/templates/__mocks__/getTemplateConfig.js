'use strict'

const mockConfig = {
  commitTemplate: '.git-commit-message',
  dependencyName: '',
  far: [
    'CONTRIBUTING.md',
    'LICENSE',
    'README.md'
  ],
  filesDir: 'tmpl',
  manifestFile: 'pkg.json',
  name: 'Mock Template',
  rename: {
    '.gitignore.template': '.gitignore',
    '.npmrc.template': '.npmrc'
  }
}

const mockGetTemplateConfig = jest.fn(
  _ => JSON.parse(JSON.stringify(mockConfig))
)

mockGetTemplateConfig.__returnOnce = config => {
  mockGetTemplateConfig.mockReturnValueOnce(config)
}

module.exports = exports = mockGetTemplateConfig
