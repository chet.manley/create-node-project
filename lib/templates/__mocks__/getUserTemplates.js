'use strict'

const mockTemplates = {
  config: {
    defaults: {
      manifestFile: 'pkg.json',
      templateName: 'base',
      templateFilesDir: 'files'
    }
  },
  path: '/tmp/mock-user-templates',
  templateDirs: ['base', 'tmpl1', 'tmpl2']
}

const mockGetUserTemplates = jest.fn(
  async _ => JSON.parse(JSON.stringify(mockTemplates))
)

mockGetUserTemplates.__returnOnce = config => {
  mockGetUserTemplates.mockReturnValueOnce(config)
}

module.exports = exports = mockGetUserTemplates
