'use strict'

const templates = {
  config: {},
  list: [],
  path: '',
  templateDirs: [],
  get available () {
    return this.list.map(tmpl => tmpl.name || tmpl.baseName)
  },
  getTemplate (template = null) {
    return this.list.find(
      ({ baseName, name }) => name === template || baseName === template
    ) || null
  }
}

module.exports = exports = templates
