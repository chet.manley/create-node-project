'use strict'

const { createDictionary } = require('../../utils')

async function createFarDictionary ([config, installConfig]) {
  const { manifest } = installConfig
  const user = { ...config.gitUser }
  const [namespace, slug] = manifest.name.includes('/')
    ? manifest.name.split('/').map(v => v.replace('@', ''))
    : [user.mention, manifest.name]

  installConfig.dictionary = createDictionary({
    project: {
      description: manifest.description,
      name: manifest.name,
      namespace,
      slug,
      urlPath: manifest.name.includes('@')
        ? manifest.name.replace('@', '')
        : `${user.mention}/${manifest.name}`
    },
    user,
    year: new Date().getFullYear()
  })

  return [config, installConfig]
}

module.exports = exports = createFarDictionary
