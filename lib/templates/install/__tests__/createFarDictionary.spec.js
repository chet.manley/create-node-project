'use strict'

const createFarDictionary = require('../createFarDictionary.js')

const mockNamespace = 'mock-ns'
const mockPackageName = 'mock-package-name'
const mockUsername = 'user.name'
const mockConfig = {
  gitUser: {
    mention: mockUsername
  }
}
const mockInstallConfig = {
  manifest: {
    name: mockPackageName,
    description: ''
  }
}
const mockInstallConfigWithNamespace = {
  manifest: {
    name: `@${mockNamespace}/${mockPackageName}`,
    description: ''
  }
}

describe('createFarDictionary AsyncFactory dictionary Object', () => {
  beforeEach(() => jest.clearAllMocks())

  describe('dictionary Object', () => {
    it('should resolve non-namespaced manifests', async () => {
      const [, { dictionary }] = await createFarDictionary([
        mockConfig,
        mockInstallConfig
      ])

      expect(dictionary).toHaveProperty('project.namespace', mockUsername)
      expect(dictionary).toHaveProperty('project.slug', mockPackageName)
      expect(dictionary).toHaveProperty(
        'project.urlPath',
        `${mockUsername}/${mockPackageName}`
      )
    })

    it('should resolve namespaced manifests', async () => {
      const [, { dictionary }] = await createFarDictionary([
        mockConfig,
        mockInstallConfigWithNamespace
      ])

      expect(dictionary).toHaveProperty('project.namespace', mockNamespace)
      expect(dictionary).toHaveProperty('project.slug', mockPackageName)
      expect(dictionary).toHaveProperty(
        'project.urlPath',
        `${mockNamespace}/${mockPackageName}`
      )
    })
  })
})
