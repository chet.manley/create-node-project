'use strict'

const { readFileSpy, writeFileSpy } = require('fs').promises
const { vol } = require('memfs')
const { join } = require('path')

const { createDictionary } = require('../../../utils')

const fillPlaceholders = require('../fillPlaceholders.js')

const mockDictionary = createDictionary({
  key1: 'value1',
  key2: {
    key3: 'value3'
  }
})
const mockTargetPath = '/tmp'
const mockFileName = 'file'
const mockFilePath = join(mockTargetPath, mockFileName)
const mockConfig = { targetPath: mockTargetPath }
const mockInstallConfig = {
  dictionary: mockDictionary,
  files: {
    fill: [mockFileName],
    filled: []
  }
}

describe('fillPlaceholders AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    mockInstallConfig.files.fill = [mockFileName]
    mockInstallConfig.files.filled = []
  })

  afterEach(() => vol.reset())

  it('should resolve with input when there are no files to fill', async () => {
    mockInstallConfig.files.fill = []

    await expect(fillPlaceholders([mockConfig, mockInstallConfig]))
      .resolves.toEqual([mockConfig, mockInstallConfig])
    expect(readFileSpy).toHaveBeenCalledTimes(0)
    expect(writeFileSpy).toHaveBeenCalledTimes(0)
  })

  it('should replace placeholders with single keys', async () => {
    vol.fromJSON({
      file: 'test {{ key1 }} test.'
    }, mockTargetPath)

    const fileOutput = `test ${mockDictionary.key1} test.`
    const [, { files }] = await fillPlaceholders([
      mockConfig,
      mockInstallConfig
    ])

    expect(files).toHaveProperty('filled', [mockFileName])
    expect(vol.toJSON()).toHaveProperty(mockFilePath, fileOutput)
    expect(readFileSpy).toHaveBeenCalledTimes(1)
    expect(writeFileSpy).toHaveBeenCalledTimes(1)
  })

  it('should replace placeholders with key paths', async () => {
    vol.fromJSON({
      file: 'test {{ key2.key3 }} test.'
    }, mockTargetPath)

    const fileOutput = `test ${mockDictionary.key2.key3} test.`
    const [, { files }] = await fillPlaceholders([
      mockConfig,
      mockInstallConfig
    ])

    expect(files).toHaveProperty('filled', [mockFileName])
    expect(vol.toJSON()).toHaveProperty(mockFilePath, fileOutput)
    expect(readFileSpy).toHaveBeenCalledTimes(1)
    expect(writeFileSpy).toHaveBeenCalledTimes(1)
  })

  it('should skip placeholders with invalid or empty keys', async () => {
    vol.fromJSON({
      file: 'test {{ key4 }} test.'
    }, mockTargetPath)

    const fileOutput = 'test {{ key4 }} test.'
    const [, { files }] = await fillPlaceholders([
      mockConfig,
      mockInstallConfig
    ])

    expect(files).toHaveProperty('filled', [])
    expect(vol.toJSON()).toHaveProperty(mockFilePath, fileOutput)
    expect(readFileSpy).toHaveBeenCalledTimes(1)
    expect(writeFileSpy).toHaveBeenCalledTimes(0)
  })
})
