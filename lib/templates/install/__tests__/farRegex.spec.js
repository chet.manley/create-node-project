'use strict'

const farRegex = require('../farRegex.js')

const mockString =
`
line1 {{ placeholder1 }}
line2 {{broken.placeholder}}
line3 no placeholder

line4 {{ placeholder2 }} fullstop.
`
const mockCaptureGroups = ['placeholder1', 'placeholder2']
const mockMatches = mockCaptureGroups.map(v => `{{ ${v} }}`)

describe('farRegex module', () => {
  it('should export a regular expression', () => {
    expect(farRegex).toBeInstanceOf(RegExp)
  })

  describe('regular expression', () => {
    it('should match all placeholders globally', () => {
      expect(mockString.match(farRegex)).toEqual(mockMatches)
    })

    it('should contain capture group with placeholder text', () => {
      const replacer = jest.fn((_, placeholder) => {
        expect(mockCaptureGroups).toContain(placeholder)
      })

      mockString.replace(farRegex, replacer)
      expect(replacer).toHaveBeenCalledTimes(mockCaptureGroups.length)
    })
  })
})
