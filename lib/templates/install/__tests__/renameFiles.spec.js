'use strict'

const { renameSpy } = require('fs').promises
const { join } = require('path')

const { vol } = require('memfs')

const { createDictionary } = require('../../../utils')

const renameFiles = require('../renameFiles.js')

const mockDictionary = createDictionary({
  file2: 'file2.new.ext',
  files: {
    file3: 'file3.new.ext'
  }
})
const mockTargetPath = '/tmp/template/tmpl'
const mockRenameFiles = {
  'file1.ext': 'file1.new.ext',
  'file2.ext': '{{ file2 }}',
  'file3.ext': '{{ files.file3 }}'
}
const mockRenamedFiles = [
  'file1.new.ext',
  'file2.new.ext',
  'file3.new.ext'
]
const mockVolume = {
  'file1.ext': '...',
  'file2.ext': '...',
  'file3.ext': '...'
}
const mockRenamedVolume = {}

for (const file of mockRenamedFiles) {
  mockRenamedVolume[join(mockTargetPath, file)] = '...'
}

const mockConfig = {
  targetPath: mockTargetPath
}
const mockInstallConfig = {
  dictionary: mockDictionary,
  files: {
    rename: mockRenameFiles,
    renamed: []
  }
}

describe('renameFiles AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    mockInstallConfig.files.rename = mockRenameFiles
    mockInstallConfig.files.renamed = []
  })

  afterEach(() => vol.reset())

  it('should reject with Error on filesystem errors', async () => {
    await expect(renameFiles([mockConfig, mockInstallConfig]))
      .rejects.toThrow()
  })

  it('should resolve with input when there are no files to fill', async () => {
    mockInstallConfig.files.rename = {}

    await expect(renameFiles([mockConfig, mockInstallConfig]))
      .resolves.toEqual([mockConfig, mockInstallConfig])
    expect(renameSpy).toHaveBeenCalledTimes(0)
  })

  it('should modify installConfig with Array of renamed files', async () => {
    vol.fromJSON(mockVolume, mockTargetPath)

    const [, { files }] = await renameFiles([
      mockConfig,
      mockInstallConfig
    ])

    expect(files).toHaveProperty('renamed', mockRenamedFiles)
    expect(vol.toJSON()).toEqual(mockRenamedVolume)
  })
})
