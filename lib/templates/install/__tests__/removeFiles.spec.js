'use strict'

const { vol } = require('memfs')

const removeFiles = require('../removeFiles.js')

const mockRemoveFiles = [
  '/tmp/file1.ext',
  '/tmp/file2.ext',
  '/tmp/file3.ext'
]
const mockVolume = {
  '/tmp/file1.ext': '...',
  '/tmp/file2.ext': '...',
  '/tmp/file3.ext': '...'
}
const mockRemovedVolume = { '/tmp': null }

const mockConfig = {}
const mockInstallConfig = {
  files: {
    remove: mockRemoveFiles,
    removed: []
  }
}

describe('removeFiles AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    mockInstallConfig.files.remove = mockRemoveFiles
    mockInstallConfig.files.removed = []
  })

  afterEach(() => vol.reset())

  it('should reject with Error on filesystem errors', async () => {
    await expect(removeFiles([mockConfig, mockInstallConfig]))
      .rejects.toThrow()
  })

  it('should resolve with input if there are no files to remove', async () => {
    mockInstallConfig.files.remove = []
    vol.fromJSON(mockVolume)

    await expect(removeFiles([mockConfig, mockInstallConfig]))
      .resolves.toEqual([mockConfig, mockInstallConfig])
    expect(vol.toJSON()).toEqual(mockVolume)
  })

  it('should modify installConfig with Array of removed files', async () => {
    vol.fromJSON(mockVolume)

    const [, { files }] = await removeFiles([
      mockConfig,
      mockInstallConfig
    ])

    expect(files).toHaveProperty('removed', mockRemoveFiles)
    expect(vol.toJSON()).toEqual(mockRemovedVolume)
  })
})
