'use strict'

const { join } = require('path')

const { vol } = require('memfs')

const copyFiles = require('../copyFiles.js')

const mockTargetPath = '/tmp/mock-project'
const mockTemplateFilesPath = '/tmp/tmpl'
const mockTemplateFiles = [
  'bin/cli',
  'lib/index.js',
  'index.js',
  'pkg.json'
]
const mockVolume = {
  '/tmp/mock-project': null
}
const mockCopiedVolume = {}

for (const file of mockTemplateFiles) {
  mockVolume[file] = '...'
  mockCopiedVolume[join(mockTemplateFilesPath, file)] = '...'
  mockCopiedVolume[join(mockTargetPath, file)] = '...'
}

const mockConfig = {
  targetPath: mockTargetPath
}
const mockInstallConfig = {
  filesPath: mockTemplateFilesPath
}

describe('copyFiles AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  afterEach(() => vol.reset())

  it('should reject with Error on filesystem errors', async () => {
    await expect(copyFiles([mockConfig, mockInstallConfig]))
      .rejects.toThrow()
  })

  it('should resolve to input on success', async () => {
    vol.fromJSON(mockVolume, mockTemplateFilesPath)

    await expect(copyFiles([mockConfig, mockInstallConfig]))
      .resolves.toEqual([mockConfig, mockInstallConfig])
    expect(vol.toJSON()).toEqual(mockCopiedVolume)
  })
})
