'use strict'

const { join } = require('path')

const { vol } = require('memfs')

const verifyFilesPath = require('../verifyFilesPath.js')

const mockTemplatesPath = '/tmp/templates'
const mockFilesDir = 'tmpl'
const mockPath = join(mockTemplatesPath, 'tmpl1')
const mockPathNotDirectory = join(mockTemplatesPath, 'tmpl2')
const mockPathNotFound = join(mockTemplatesPath, 'tmpl3')
const mockVolume = {
  'tmpl1/tmpl/': null,
  'tmpl2/tmpl': '...',
  'tmpl3/': null
}

const mockConfig = {}
let mockInstallConfig = {}

describe('verifyFilesPath AsyncFunction', () => {
  beforeAll(() => vol.fromJSON(mockVolume, mockTemplatesPath))

  beforeEach(() => {
    jest.clearAllMocks()
    mockInstallConfig = {
      filesPath: join(mockPath, mockFilesDir),
      name: 'mock-template'
    }
  })

  afterAll(() => vol.reset())

  it('should reject with Error if path is not a directory', async () => {
    vol.fromJSON(mockVolume, mockTemplatesPath)

    mockInstallConfig.filesPath = join(mockPathNotDirectory, mockFilesDir)

    await expect(verifyFilesPath([mockConfig, mockInstallConfig]))
      .rejects.toThrow('not a directory')
  })

  it('should reject with Error if path is not found', async () => {
    vol.fromJSON(mockVolume, mockTemplatesPath)

    mockInstallConfig.filesPath = join(mockPathNotFound, mockFilesDir)

    await expect(verifyFilesPath([mockConfig, mockInstallConfig]))
      .rejects.toThrow('directory Not Found')
  })

  it('should resolve with input on success', async () => {
    vol.fromJSON(mockVolume, mockTemplatesPath)

    await expect(verifyFilesPath([mockConfig, mockInstallConfig]))
      .resolves.toEqual([mockConfig, mockInstallConfig])
  })
})
