'use strict'

const { join } = require('path')

const { vol } = require('memfs')

const mergeManifest = require('../mergeManifest.js')

const mockManifestFile = 'pkg.json'
const mockMergedManifestFiles = {
  name: '@mock/package',
  description: 'mock package'
}
const mockTemplatePath = '/tmp/template'
const mockTargetPath = join(mockTemplatePath, 'tmpl')
const mockRemovefiles = [join(mockTargetPath, mockManifestFile)]
const mockVolume = {
  'tmpl/package.json': '{ "name": "@mock/package" }',
  'tmpl/pkg.json': '{ "description": "mock package" }'
}

const mockConfig = {
  targetPath: mockTargetPath
}
const mockInstallConfig = {
  files: {
    remove: []
  },
  manifestFile: mockManifestFile
}

describe('mergeManifest AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    mockInstallConfig.files.remove = []
  })

  afterEach(() => vol.reset())

  it('should reject with Error if manifest files are missing', async () => {
    await expect(mergeManifest([mockConfig, mockInstallConfig]))
      .rejects.toThrow()
  })

  it('should modify installConfig with merged package manifest', async () => {
    vol.fromJSON(mockVolume, mockTemplatePath)

    const [, installConfig] = await mergeManifest([
      mockConfig,
      mockInstallConfig
    ])

    expect(installConfig).toHaveProperty('manifest', mockMergedManifestFiles)
    expect(vol.toJSON()).toHaveProperty(
      [join(mockTargetPath, 'package.json')],
      JSON.stringify(mockMergedManifestFiles, null, 2)
    )
  })

  it(
    'should modify installConfig with Array of files to be removed',
    async () => {
      vol.fromJSON(mockVolume, mockTemplatePath)

      const [, { files }] = await mergeManifest([mockConfig, mockInstallConfig])

      expect(files).toHaveProperty('remove', mockRemovefiles)
      expect(vol.toJSON()).toHaveProperty(
        [join(mockTargetPath, 'package.json')],
        JSON.stringify(mockMergedManifestFiles, null, 2)
      )
    })
})
