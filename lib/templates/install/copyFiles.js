'use strict'

const { copyRecursive } = require('../../utils/fs')

async function copyFiles ([config, installConfig]) {
  const { filesPath } = installConfig

  await copyRecursive(filesPath, config.targetPath)

  return [config, installConfig]
}

module.exports = exports = copyFiles
