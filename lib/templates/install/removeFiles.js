'use strict'

const { unlink } = require('fs').promises

async function removeFiles ([config, installConfig]) {
  const { files } = installConfig

  for (const file of files.remove) {
    await unlink(file)
    files.removed.push(file)
  }

  return [config, installConfig]
}

module.exports = exports = removeFiles
