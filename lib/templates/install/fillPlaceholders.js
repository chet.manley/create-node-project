'use strict'

const { writeFile } = require('fs').promises
const { join } = require('path')

const { createFindAndReplace } = require('../../utils')
const { readTextFile } = require('../../utils/fs')

const farRegex = require('./farRegex')

async function fillPlaceholders ([config, installConfig]) {
  const { dictionary, files } = installConfig
  const far = createFindAndReplace(farRegex, dictionary)

  for (const file of files.fill) {
    const filePath = join(config.targetPath, file)
    const contents = await readTextFile(filePath)
    const replaced = far(contents)

    if (replaced !== contents) {
      await writeFile(filePath, replaced, 'utf8')
      files.filled.push(file)
    }
  }

  return [config, installConfig]
}

module.exports = exports = fillPlaceholders
