'use strict'

const { rename } = require('fs').promises
const { join, parse } = require('path')

const farRegex = require('./farRegex')

async function renameFiles ([config, installConfig]) {
  const { dictionary, files } = installConfig

  for (const [fileName, newName] of Object.entries(files.rename)) {
    const filePath = join(config.targetPath, fileName)
    const key = new RegExp(farRegex).exec(newName)
    const newFileName = key ? dictionary._find(key[1]) : newName
    if (!newFileName) { continue }
    const newFilePath = join(parse(filePath).dir, newFileName)

    await rename(filePath, newFilePath)
    files.renamed.push(newFileName)
  }

  return [config, installConfig]
}

module.exports = exports = renameFiles
