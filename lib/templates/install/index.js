'use strict'

const install = {
  copyFiles: require('./copyFiles'),
  createFarDictionary: require('./createFarDictionary'),
  fillPlaceholders: require('./fillPlaceholders'),
  mergeManifest: require('./mergeManifest'),
  removeFiles: require('./removeFiles'),
  renameFiles: require('./renameFiles'),
  verifyFilesPath: require('./verifyFilesPath')
}

module.exports = exports = install
