'use strict'

const { isDirectory } = require('../../utils/fs')

async function verifyFilesPath ([config, installConfig]) {
  const { filesPath, name } = installConfig
  const errorPrefix = `Template '${name}'`

  try {
    if (!await isDirectory(filesPath)) {
      throw new Error(`${errorPrefix} files path is not a directory`)
    }
  } catch (error) {
    if (error.code === 'ENOENT') {
      throw new Error(`${errorPrefix} files directory Not Found`)
    }
    if (error.code === 'EACCES') {
      throw new Error(`${errorPrefix} files directory Permission Denied`)
    }
    throw error
  }

  return [config, installConfig]
}

module.exports = exports = verifyFilesPath
