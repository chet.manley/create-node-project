'use strict'

const { writeFile } = require('fs').promises
const { join } = require('path')

const deepmerge = require('deepmerge')
const sortPackageJson = require('sort-package-json')

const { parseJsonFile } = require('../../utils/fs')

async function mergeManifest ([config, installConfig]) {
  const { files, manifestFile } = installConfig
  const originalPkgPath = join(config.targetPath, 'package.json')
  const templatePkgPath = join(config.targetPath, manifestFile)
  const originalPkgJson = await parseJsonFile(originalPkgPath)
  const templatePkgJson = await parseJsonFile(templatePkgPath)
  const mergedPkgJson = sortPackageJson(
    deepmerge(originalPkgJson, templatePkgJson),
    {
      sortOrder: [
        'name',
        'version',
        'description',
        'type',
        'main',
        'bin',
        'scripts',
        'files'
      ]
    }
  )

  await writeFile(
    originalPkgPath,
    JSON.stringify(mergedPkgJson, null, 2),
    'utf8'
  )

  installConfig.manifest = mergedPkgJson
  files.remove.push(templatePkgPath)

  return [config, installConfig]
}

module.exports = exports = mergeManifest
