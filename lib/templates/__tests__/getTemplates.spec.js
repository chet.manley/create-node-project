'use strict'

describe('getTemplates AsyncFactory', () => {
  jest.doMock('../getUserTemplates.js')
  let getTemplates = null
  let mockGetUserTemplates = null

  beforeEach(() => {
    jest.clearAllMocks()
    jest.resetModules()
    getTemplates = require('../getTemplates.js')
    mockGetUserTemplates = require('../getUserTemplates.js')
  })

  afterAll(() => jest.resetModules())

  it('should reject with Error if no templates are found', async () => {
    mockGetUserTemplates.__returnOnce({ templateDirs: [] })

    await expect(getTemplates('/tmp/nodir'))
      .rejects.toThrow('No templates found')
  })

  it('should resolve to node-project-templates if missing arguments', async () => {
    const mockTemplates = require('@chet.manley/node-project-templates')
    const templates = await getTemplates()

    expect(templates).toEqual(expect.objectContaining(mockTemplates))
    expect(templates).toHaveProperty('available', mockTemplates.templateDirs)
    expect(templates).toHaveProperty('path', mockTemplates.path)
  })

  it('should resolve to user templates if templatesDir passed', async () => {
    const mockTemplates = await mockGetUserTemplates()
    const templates = await getTemplates(mockTemplates.path)

    expect(templates).toEqual(expect.objectContaining(mockTemplates))
    expect(templates).toHaveProperty('available', mockTemplates.templateDirs)
  })

  it('should resolve with cached templates on subsequent calls', async () => {
    const mockTemplates = await mockGetUserTemplates()
    const templates1 = await getTemplates(mockTemplates.path)
    const templates2 = await getTemplates()

    expect(templates2).toBe(templates1)
  })

  it('resolved templates#list should be Template[]', async () => {
    const Template = require('../Template.js')

    await expect(getTemplates())
      .resolves.toHaveProperty(
        'list',
        expect.arrayContaining([expect.any(Template)])
      )
  })
})
