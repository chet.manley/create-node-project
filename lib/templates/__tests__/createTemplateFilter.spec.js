'use strict'

const { readdir } = require('fs').promises
const { vol } = require('memfs')

const createTemplateFilter = require('../createTemplateFilter.js')

const templatesPath = '/tmp/templates'
const mockVolume = {
  './tmpl0': null,
  './tmpl1/file.ext': '',
  './tmpl2/file.ext': '',
  './tmpl.ext': ''
}
const mockResults = ['tmpl1', 'tmpl2']

describe('createTemplateFilter Factory filter function', () => {
  const filter = createTemplateFilter(templatesPath)

  beforeEach(() => {
    jest.clearAllMocks()
    vol.fromJSON(mockVolume, templatesPath)
  })

  afterEach(() => vol.reset())

  it('should reject with TypeError if missing argument', async () => {
    await expect(_ => filter()).rejects.toThrow(TypeError)
  })

  it('should resolve to Array of non-empty directories', async () => {
    const files = await readdir(templatesPath)

    await expect(filter(files)).resolves.toEqual(mockResults)
  })
})
