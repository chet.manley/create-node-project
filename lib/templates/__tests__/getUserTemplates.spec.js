'use strict'

const { join } = require('path')

const { vol } = require('memfs')

const getUserTemplates = require('../getUserTemplates.js')

const mockTemplatesDirAbs = '/tmp/templates'
const mockTemplatesDirRel = 'templates'
const mockPaths = ['/tmp']
const mockConfigModule = join(mockTemplatesDirAbs, 'config')
const mockIndexModule = join(mockTemplatesDirAbs, '')

const mockConfig = { key1: 'value1' }
const mockError = new Error('mock error')
const mockTemplates = {
  config: mockConfig,
  path: mockTemplatesDirAbs,
  templateDirs: ['tmpl1', 'tmpl2']
}
const mockVolume = {
  'config.json': '',
  'index.js': '',
  'tmpl1/config.json': '{}',
  'tmpl1/tmpl/index.js': '',
  'tmpl2/config.json': '{}',
  'tmpl2/tmpl/index.js': '',
  'tmpl3/': null
}

describe('getUserTemplates AsyncFunction', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    jest.doMock(mockConfigModule, _ => mockConfig, { virtual: true })
    jest.doMock(mockIndexModule, _ => mockTemplates, { virtual: true })
  })

  afterEach(() => {
    mockError.code = undefined
    jest.resetModules()
  })

  afterAll(() => {
    jest.unmock(mockConfigModule)
    jest.unmock(mockIndexModule)
  })

  it('should reject with custom Error if templatesDir is missing', async () => {
    const expectedError = new Error('User templates directory not found')

    await expect(getUserTemplates()).rejects.toThrow(expectedError)
    await expect(getUserTemplates(undefined, mockPaths))
      .rejects.toThrow(expectedError)
  })

  it('should reject with custom Error if access is denied', async () => {
    mockError.code = 'EACCES'
    jest.doMock(
      mockIndexModule,
      _ => { throw mockError },
      { virtual: true }
    )

    await expect(getUserTemplates(mockTemplatesDirAbs))
      .rejects.toThrow('Permission denied accessing user templates')
  })

  it('should reject with errors thrown by require()', async () => {
    mockError.code = 'EMOCK'
    jest.doMock(
      mockIndexModule,
      _ => { throw mockError },
      { virtual: true }
    )

    await expect(getUserTemplates(mockTemplatesDirAbs))
      .rejects.toThrow(mockError)
  })

  it('should resolve if a module index is found', async () => {
    await expect(getUserTemplates(mockTemplatesDirAbs))
      .resolves.toEqual(mockTemplates)
  })

  it('should resolve if a template config is found', async () => {
    vol.fromJSON(mockVolume, mockTemplatesDirAbs)
    jest.unmock(mockIndexModule)

    await expect(getUserTemplates(mockTemplatesDirAbs))
      .resolves.toEqual(mockTemplates)
  })

  it('should resolve if provided a relative path', async () => {
    await expect(getUserTemplates(mockTemplatesDirRel, mockPaths))
      .resolves.toEqual(mockTemplates)
  })
})
