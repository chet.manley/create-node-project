'use strict'

const { join } = require('path')

const getTemplateConfig = require('../getTemplateConfig.js')

const targetPath = '/tmp'
const mockConfig = { key1: null }
const mockConfigModule = join(targetPath, 'config')
const mockErrorMessage = 'mock error'

describe('getTemplateConfig Function', () => {
  beforeEach(() => jest.clearAllMocks())

  afterAll(() => jest.unmock(mockConfigModule))

  it('should throw TypeError if missing argument', () => {
    /**
      * path.join does not actually throw an instance of TypeError
      * https://nodejs.org/api/path.html#path_path_join_paths
      * https://repl.it/repls/HilariousWarmDrawings
      * expect(_ => getTemplateConfig()).toThrow(TypeError)
      *
      * Node v10 throws a different error message than v12+
      */
    const pathJoinError = 'The "path" argument must be of type string.'

    expect(_ => getTemplateConfig()).toThrow(pathJoinError)
  })

  it('should throw custom Error if access is denied', () => {
    const mockError = new Error(mockErrorMessage)
    mockError.code = 'EACCES'
    jest.doMock(
      mockConfigModule,
      _ => { throw mockError },
      { virtual: true }
    )

    expect(_ => getTemplateConfig(targetPath))
      .toThrow('Permission denied reading template config')
  })

  it('should re-throw errors thrown by require()', () => {
    const mockError = new Error(mockErrorMessage)
    mockError.code = 'EMOCK'
    jest.doMock(
      mockConfigModule,
      _ => { throw mockError },
      { virtual: true }
    )

    expect(_ => getTemplateConfig(targetPath)).toThrow(mockErrorMessage)
  })

  it('should return empty object if config not found', () => {
    jest.unmock(mockConfigModule)

    expect(getTemplateConfig(targetPath)).toEqual({})
  })

  it('should return config object', () => {
    jest.doMock(mockConfigModule, _ => mockConfig, { virtual: true })

    expect(getTemplateConfig(targetPath)).toEqual(mockConfig)
  })
})
