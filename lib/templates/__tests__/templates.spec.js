'use strict'

const templates = require('../templates.js')

const mockAvailable = ['tmpl1', 'tmpl2']
const mockTemplate1 = { name: 'tmpl1' }
const mockTemplate2 = { baseName: 'tmpl2' }

templates.list = [
  mockTemplate1,
  mockTemplate2
]

describe('templates Object', () => {
  afterAll(() => jest.resetModules())

  it('#available getter should be Array of template names', () => {
    expect(templates).toHaveProperty('available', mockAvailable)
  })

  it('#getTemplate() should return Template or undefined', () => {
    expect(templates.getTemplate('tmpl1')).toBe(mockTemplate1)
    expect(templates.getTemplate('tmpl2')).toBe(mockTemplate2)
    expect(templates.getTemplate('tmpl3')).toBeNull()
    expect(templates.getTemplate()).toBeNull()
  })
})
