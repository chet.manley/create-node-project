'use strict'

const { join } = require('path')

const mockNcu = require('npm-check-updates')

jest.mock('../getTemplateConfig.js')
const mockGetTemplateConfig = require('../getTemplateConfig.js')

const Template = require('../Template.js')

const mockBaseName = 'mock-template'
const mockConfig = mockGetTemplateConfig()
const mockDefaults = {
  manifestFile: 'manifest.json',
  templateFilesDir: 'files'
}
const mockTemplatePath = join('/tmp/templates/', mockBaseName)

describe('Template Class', () => {
  let template = null

  beforeEach(() => {
    jest.clearAllMocks()
    template = new Template(mockTemplatePath, mockDefaults)
  })

  it('#baseName should equal basename of template path', () => {
    expect(template.baseName).toBe(mockBaseName)
  })

  it('#config should equal template configuration object', () => {
    expect(template.config).toEqual(mockConfig)
  })

  it('#filesPath can equal configured files path', () => {
    const mockTemplateFilesPath = join(mockTemplatePath, mockConfig.filesDir)

    expect(template.filesPath).toBe(mockTemplateFilesPath)
  })

  it('#filesPath can equal default files path', () => {
    const config = mockGetTemplateConfig()
    const defaultTemplateFilesPath = join(
      mockTemplatePath,
      mockDefaults.templateFilesDir
    )
    config.filesDir = undefined
    mockGetTemplateConfig.__returnOnce(config)
    template = new Template(mockTemplatePath, mockDefaults)

    expect(template.filesPath).toBe(defaultTemplateFilesPath)
  })

  it('#manifestFile can equal #config.manifestFile', () => {
    expect(template.manifestFile).toBe(mockConfig.manifestFile)
  })

  it('#manifestFile can equal defaults.manifestFile', () => {
    const config = mockGetTemplateConfig()
    config.manifestFile = undefined
    mockGetTemplateConfig.__returnOnce(config)
    template = new Template(mockTemplatePath, mockDefaults)

    expect(template.manifestFile).toBe(mockDefaults.manifestFile)
  })

  it('#name can equal #config.name', () => {
    expect(template.name).toBe(mockConfig.name)
  })

  it('#name can equal #baseName', () => {
    const config = mockGetTemplateConfig()
    config.name = undefined
    mockGetTemplateConfig.__returnOnce(config)
    template = new Template(mockTemplatePath, mockDefaults)

    expect(template.name).toBe(mockBaseName)
  })

  it('#path should equal template path', () => {
    expect(template.path).toBe(mockTemplatePath)
  })

  it('#update() async method should update manifest file', async () => {
    mockNcu.__setResults(2)

    await expect(template.update()).resolves.toBe('Applied 2 updates')
  })
})
