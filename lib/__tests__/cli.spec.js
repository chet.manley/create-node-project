'use strict'

jest.mock('../config/createConfig.js')
jest.mock('../tasks/getTasks.js')

const cli = require('../cli.js')

const ENV = JSON.parse(JSON.stringify(process.env))
const noop = _ => null
const consoleDebugSpy = jest.spyOn(console, 'debug')
  .mockImplementation(noop)
const consoleErrorSpy = jest.spyOn(console, 'error')
  .mockImplementation(noop)

describe('cli AsyncFunction', () => {
  beforeEach(() => {
    process.env = { ...ENV }
    jest.clearAllMocks()
  })

  it('returns 0 exitCode on success', async () => {
    expect(await cli([]) === 0).toBe(true)
  })

  it('returns >0 exitCode on failure', async () => {
    expect(await cli(['--fail']) > 0).toBe(true)
  })

  describe('handles errors gracefully', () => {
    it('should output Error#message to console.error', async () => {
      expect(await cli(['--fail']) > 0).toBe(true)
      expect(consoleDebugSpy).not.toHaveBeenCalled()
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1)
      expect(consoleErrorSpy).toHaveBeenLastCalledWith(
        expect.stringContaining('config ERROR')
      )
    })

    it('should output Error to console.debug on debug', async () => {
      expect(await cli(['--debug', '--fail-run']) > 0).toBe(true)
      expect(consoleErrorSpy).not.toHaveBeenCalled()
      expect(consoleDebugSpy).toHaveBeenCalledTimes(1)
      expect(consoleDebugSpy).toHaveBeenLastCalledWith(
        'DEBUG',
        expect.any(Error)
      )
    })

    it('should catch task runner errors', async () => {
      expect(await cli(['--fail-run']) > 0).toBe(true)
      expect(consoleDebugSpy).not.toHaveBeenCalled()
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1)
      expect(consoleErrorSpy).toHaveBeenLastCalledWith(
        expect.stringContaining('runner ERROR')
      )
    })

    it('should catch task runner rejects', async () => {
      expect(await cli(['--reject-run']) > 0).toBe(true)
      expect(consoleDebugSpy).not.toHaveBeenCalled()
      expect(consoleErrorSpy).toHaveBeenCalledTimes(1)
      expect(consoleErrorSpy).toHaveBeenLastCalledWith(
        expect.stringContaining('runner ERROR')
      )
    })
  })
})
