'use strict'

const createGitUser = require('./createGitUser')

function initializeRepository () {
  return {
    title: 'Initialize repository',
    skip: ctx => ctx.gitInit ? false : 'skipped with --no-git-init flag',
    task: (ctx, task) => task.newListr([
      {
        title: 'Git init repository',
        task: async (_, task) => {
          task.output = await ctx.commands.git.init()
        },
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        title: 'Add remote origin',
        skip: ctx => ctx.repository ? false : 'No repository URL provided',
        task: async (_, task) => {
          await ctx.commands.git.addOrigin(ctx.repository)
          task.output = `Added ${ctx.repository}`
        },
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        title: 'Gather git user info',
        task: async (_, task) => {
          ctx.gitUser = await createGitUser(ctx.commands.git)
          task.output = `Found ${ctx.gitUser.name} (@${ctx.gitUser.mention})`
        },
        options: { persistentOutput: ctx.verbose > 1 }
      }
    ])
  }
}

module.exports = exports = initializeRepository
