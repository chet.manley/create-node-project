'use strict'

const error = new Error('runner ERROR')

const getTasks = jest.fn(config => {
  return {
    run: _ => {
      if (config.fail) { throw error }
      return config.reject ? Promise.reject(error) : Promise.resolve(true)
    }
  }
})

module.exports = exports = getTasks
