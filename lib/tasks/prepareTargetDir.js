'use strict'

const {
  createDirRecursive,
  isDirectory,
  isEmptyDirectory
} = require('../utils/fs')

function prepareTargetDir ({ verbose }) {
  return {
    title: 'Prepare target directory',
    task: async (ctx, task) => {
      try {
        if (await isDirectory(ctx.targetPath)) {
          if (!await isEmptyDirectory(ctx.targetPath)) {
            throw new Error('Target directory is not empty')
          }
          return (task.output = 'Target directory exists')
        }
      } catch (error) {
        if (error.code === 'EACCES') {
          throw new Error('Permission denied enumerating target directory')
        }
        if (error.code !== 'ENOENT') { throw error }
      }

      try {
        await createDirRecursive(ctx.targetPath)
        return (task.output = 'Target directory created')
      } catch (error) {
        if (error.code === 'EACCES') {
          throw new Error('Permission denied creating target directory')
        }
        throw error
      }
    },
    options: { persistentOutput: verbose > 1 }
  }
}

module.exports = exports = prepareTargetDir
