'use strict'

function updateTemplates () {
  return {
    title: 'Update all package dependencies',
    task: (ctx, task) => task.newListr(
      ctx.templates.list.map(template =>
        ({
          title: template.name,
          task: async (_, task) => {
            task.output = 'Searching for updates...'
            task.output = await template.update()
              .catch(error => {
                task.output = 'Failed'

                if (error.code === 'ENOENT') {
                  return task.skip('Missing package manifest')
                }
                if (error.code === 'EACCES') {
                  throw new Error('Permission denied reading package manifest')
                }

                throw error
              })
          },
          options: { persistentOutput: ctx.verbose > 1 }
        })
      ), { exitOnError: false }
    )
  }
}

module.exports = exports = updateTemplates
