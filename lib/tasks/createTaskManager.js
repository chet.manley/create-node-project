'use strict'

const { Listr } = require('listr2')

function createTaskManager (tasks = [], config) {
  const collapse = config.verbose < 1
  const collapseErrors = config.verbose < 1
  const collapseSkips = config.verbose < 2
  const options = {
    concurrent: false,
    ctx: config,
    exitOnError: true,
    rendererOptions: {
      collapse,
      collapseErrors,
      collapseSkips,
      showErrorMessage: !collapseErrors,
      showSkipMessage: !collapseSkips
    }
  }
  return new Listr(tasks, options)
}

module.exports = exports = createTaskManager
