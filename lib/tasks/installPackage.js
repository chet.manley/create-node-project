'use strict'

const { getStringFromMatches } = require('../utils')

function initializePackage () {
  return {
    title: 'Install package',
    skip: ctx => ctx.npmInstall ? false : 'Skipped with --no-npm-install flag',
    task: (ctx, task) => task.newListr([
      {
        title: 'NPM install',
        task: async (_, task) => {
          const stdout = await ctx.commands.npm.install()
          const added = stdout.match(/^added.*packages.*$/m)
          const found = stdout.match(/^found.*vuln.*$/m)
          task.output = getStringFromMatches(added, found)
          ctx.vulnerabilities = found ? +(found[0].match(/\d+/) || [0])[0] : 0
        },
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        title: 'NPM audit fix',
        skip: ctx => ctx.vulnerabilities > 0 ? false : 'No vulnerabilities found',
        task: async (_, task) => {
          const stdout = await ctx.commands.npm.auditFix()
          const added = stdout.match(/^added.*packages.*$/m)
          const fixed = stdout.match(/^fixed.*vuln.*$/m)
          const vulns = stdout.match(/^\s*?[\d]+\spackage.*$/m)
          task.output = getStringFromMatches(added, fixed, vulns)
        },
        options: { persistentOutput: ctx.verbose > 1 }
      }
    ])
  }
}

module.exports = exports = initializePackage
