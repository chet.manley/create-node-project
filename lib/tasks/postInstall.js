'use strict'

function postInstall () {
  return [{
    title: 'Git post-install',
    skip: ctx => ctx.gitInit ? false : 'skipped with --no-git-init option',
    task: (ctx, task) => task.newListr([
      {
        title: 'Set commit template',
        skip: _ => ctx.commitTemplate ? false : 'No commit template provided',
        task: async (_, task) => {
          task.output = await ctx.commands.git.config.set(
            'commit.template',
            ctx.commitTemplate
          )
        },
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        title: 'Add all files',
        task: async _ => await ctx.commands.git.addAll(),
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        /** TODO
         * Find a way to prompt for GPG key
         */
        title: 'Initial commit',
        task: async _ => await ctx.commands.git.commit(ctx.commitMessage),
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        title: `Checkout ${ctx.checkoutBranch} branch`,
        skip: _ => ctx.checkoutBranch ? false : 'No checkout branch provided',
        task: async (_, task) => {
          task.output = await ctx.commands.git.checkout.branch(
            ctx.checkoutBranch
          )
        },
        options: { persistentOutput: ctx.verbose > 1 }
      },
      {
        title: `Push ${ctx.checkoutBranch} branch`,
        skip: _ => {
          if (!ctx.checkoutBranch) { return 'No checkout branch provided' }
          if (!ctx.repository) { return 'No repository URL provided' }
          return false
        },
        task: async _ => await ctx.commands.git.push.branch(ctx.checkoutBranch),
        options: { persistentOutput: ctx.verbose > 1 }
      }
    ])
  },
  {
    task: (ctx, task) => {
      task.output = `Project '${
        ctx.projectName
      }' created from '${
        ctx.templateName
      }' template`
    },
    options: { bottomBar: true, persistentOutput: true }
  }]
}

module.exports = exports = postInstall
