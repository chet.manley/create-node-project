'use strict'

jest.mock('../../config/createGitCommands.js')
const mockGit = require('../../config/createGitCommands.js')()

const createGitUser = require('../createGitUser.js')

const mockEmail = 'mock-email@domain.tld'
const mockMention = '@mock.username'
const mockName = 'Mock User Name'
const mockGitUser = {
  email: mockEmail,
  mention: mockMention,
  name: mockName
}

describe('createGitUser AsyncFactory', () => {
  beforeEach(() => jest.clearAllMocks())

  it('should resolve to git user Object', async () => {
    await expect(createGitUser(mockGit)).resolves.toEqual(mockGitUser)
  })
})
