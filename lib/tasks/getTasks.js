'use strict'

const createTaskManager = require('./createTaskManager')
const initializePackage = require('./initializePackage')
const initializeRepository = require('./initializeRepository')
const installPackage = require('./installPackage')
const installTemplate = require('./installTemplate')
const postInstall = require('./postInstall')
const prepareTargetDir = require('./prepareTargetDir')
const updatePackage = require('./updatePackage')
const updateTemplates = require('./updateTemplates')

function getTasks (config) {
  if (config.updateAll) {
    return createTaskManager([
      updateTemplates()
    ], config)
  }

  return createTaskManager([
    prepareTargetDir(config),
    initializeRepository(),
    initializePackage(config),
    installTemplate(config),
    updatePackage(config),
    installPackage(),
    ...postInstall()
  ], config)
}

module.exports = exports = getTasks
