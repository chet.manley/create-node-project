'use strict'

function initializeRepository ({ verbose }) {
  return {
    title: 'Initialize package',
    task: async (ctx, task) => {
      const stdout = await ctx.commands.npm.init()
      task.output = ctx.verbose > 2 ? stdout : stdout.split('\n')[0]
    },
    options: { persistentOutput: verbose > 1 }
  }
}

module.exports = exports = initializeRepository
