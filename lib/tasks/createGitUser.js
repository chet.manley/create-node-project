'use strict'

async function createGitUser (git) {
  return {
    email: await git.config.get('user.email'),
    mention: (await git.config.getRegexp('credential.*username')).split(' ')[1],
    name: await git.config.get('user.name')
  }
}

module.exports = exports = createGitUser
