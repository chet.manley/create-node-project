'use strict'

const { updateDependencies } = require('../utils')

function updatePackage ({ verbose }) {
  return {
    title: 'Update package manifest',
    skip: ctx => ctx.update ? false : 'Enable with [-u, --update] flag',
    task: async (ctx, task) => {
      task.output = await updateDependencies(ctx.targetPath)
    },
    options: { persistentOutput: verbose > 1 }
  }
}

module.exports = exports = updatePackage
