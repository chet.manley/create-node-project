'use strict'

const { installTemplates } = require('../templates')

function installTemplate ({ verbose }) {
  return {
    title: 'Install template',
    task: async ctx => {
      await installTemplates(ctx)
    },
    options: { persistentOutput: verbose > 1 }
  }
}

module.exports = exports = installTemplate
