# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0...v1.0.1) (2020-12-12)


### Bug Fixes

* **docs:** spelling ([7a9c8f7](https://gitlab.com/chet.manley/create-node-project/commit/7a9c8f783b32fe131423cde480a4fe55344e0d2d))
* **docs:** toc generation skipping headers in quote blocks ([b2507ae](https://gitlab.com/chet.manley/create-node-project/commit/b2507ae62c3ae3575044239ad2c56df7b7412111))

## [1.0.0](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.7...v1.0.0) (2020-11-26)


### Bug Fixes

* **bug:** remove silent flag from npm install command, npm v7 outputs nothing in silent mode ([5e456d6](https://gitlab.com/chet.manley/create-node-project/commit/5e456d6a40e53f2f88c8891569bf114a04e4bc17))
* **bug:** replace regex for npm v7 output ([84dd6ff](https://gitlab.com/chet.manley/create-node-project/commit/84dd6ffe8900e682266fc1cbfec6030236904fc0))

## [1.0.0-alpha.7](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.6...v1.0.0-alpha.7) (2020-11-25)


### Bug Fixes

* **bug:** async function constructors are readonly ([028cf72](https://gitlab.com/chet.manley/create-node-project/commit/028cf724905d6956585f391b850104b79950f40c))
* **bug:** await createGitUser function ([9ffaf4f](https://gitlab.com/chet.manley/create-node-project/commit/9ffaf4fd01ce01b902d9154baca3957d4861fc02))
* **bug:** change createFarDictionary to AsyncFunction ([c7d3b0b](https://gitlab.com/chet.manley/create-node-project/commit/c7d3b0b0b3a8046d0bd0be4cdaae39b983ab2e74))
* **bug:** fetch user templates with or without index.js ([641d6ea](https://gitlab.com/chet.manley/create-node-project/commit/641d6ea3b37579ae00881ed03c0ceda22ca20b70))
* **bug:** initialize exit code to zero ([20abbe2](https://gitlab.com/chet.manley/create-node-project/commit/20abbe20fc31755b0f570cc6669e8a9aac1de67c))
* **bug:** misspelled export  property name ([3dd69bd](https://gitlab.com/chet.manley/create-node-project/commit/3dd69bd857630851e92635f4a679674e81aac890))
* **bug:** misspelled object property ([cebcae7](https://gitlab.com/chet.manley/create-node-project/commit/cebcae733d88ac30b8c389ddecf1c0b0039277d3))
* **bug:** node v10 does not support String.matchAll ([d84c219](https://gitlab.com/chet.manley/create-node-project/commit/d84c2198f10f1296c260d617642530fe78f82699))
* **bug:** prevent deepmerge from clobbering classes, place config in task context ([2dc7bed](https://gitlab.com/chet.manley/create-node-project/commit/2dc7bed120410a953fa1a335c46edb80fab52224))
* **bug:** prevent target path from only resolving to default CWD ([fa8de5a](https://gitlab.com/chet.manley/create-node-project/commit/fa8de5a8285f5798a96abd098e4d99ae9dad5a1e))
* **bug:** prevent template defaults from overriding config ([3cfb88f](https://gitlab.com/chet.manley/create-node-project/commit/3cfb88fa324d8cb235fd005fcca21dc7e32ce32a))
* **bug:** trim whitespace from stdout, prevent extraneous newlines at line end ([6c9330e](https://gitlab.com/chet.manley/create-node-project/commit/6c9330e5d9c52a561c83a0c4eb9b0cc30d203fd9))
* **test:** node v10 throws a different error than node v12+ ([251415e](https://gitlab.com/chet.manley/create-node-project/commit/251415e5445cac870ff74ff12866a3dcee121f32))

## [1.0.0-alpha.6](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.5...v1.0.0-alpha.6) (2020-09-16)


### Features

* push integration branch in post-install task ([da04bda](https://gitlab.com/chet.manley/create-node-project/commit/da04bda41fbb56424199417515a41a55a6d7a0c8))

## [1.0.0-alpha.5](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.4...v1.0.0-alpha.5) (2020-09-16)


### Bug Fixes

* **bug:** allow for update of non-standard package manifest files ([71a2417](https://gitlab.com/chet.manley/create-node-project/commit/71a24174e259d1409633ed64cd7cf93b61012dbb))

## [1.0.0-alpha.4](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.3...v1.0.0-alpha.4) (2020-09-15)


### Features

* allow both dictionary key paths and raw filenames for file renaming ([6f134a0](https://gitlab.com/chet.manley/create-node-project/commit/6f134a0fc1c7c671482c418d514d123b3a3e73da))
* allow template to define package manifest file ([9f4d6b3](https://gitlab.com/chet.manley/create-node-project/commit/9f4d6b3f42602b9a92dbd745791363b4b4c9b988))


### Bug Fixes

* **bug:** allow postInstall tasks to reject, suppress task stdout ([533ed35](https://gitlab.com/chet.manley/create-node-project/commit/533ed35b27488943694048a977085c86ad04f0de))
* **bug:** don't move renamed files to project root ([9d3ea68](https://gitlab.com/chet.manley/create-node-project/commit/9d3ea68ec68ad47f66cbbe66a9c91c320bf45042))
* **bug:** reject promise when exit code is not 0 ([a6734b5](https://gitlab.com/chet.manley/create-node-project/commit/a6734b5e4a6c216f9fce8e66b83350d786328a0e))
* **bug:** unquote commit message ([73ed36c](https://gitlab.com/chet.manley/create-node-project/commit/73ed36c83fe880b68d0f7b6c632b6b0452c1839d))

## [1.0.0-alpha.3](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2020-09-03)


### Bug Fixes

* **bug:** make update-all task honor verbosity setting ([05d4028](https://gitlab.com/chet.manley/create-node-project/commit/05d40280a833d5b4ad88e0e7adb0bf59448d5bf5))
* **bug:** use correct git config flag to add settings ([19da38f](https://gitlab.com/chet.manley/create-node-project/commit/19da38fc292947173b5a9866c2d666b7a2075e57))
* **ci:** trigger pipelines on master branch ([0bdc51a](https://gitlab.com/chet.manley/create-node-project/commit/0bdc51a76bd4ff6605c57652ab6d03098b8e990c))

## [1.0.0-alpha.2](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2020-08-30)


### Bug Fixes

* **ci:** add gitlab API token for tagging package after publish ([66f61a5](https://gitlab.com/chet.manley/create-node-project/commit/66f61a55fed4eb7e78b2d0a113556437ac44b66e))

## [1.0.0-alpha.1](https://gitlab.com/chet.manley/create-node-project/compare/v1.0.0-alpha.0...v1.0.0-alpha.1) (2020-08-30)


### Bug Fixes

* **ci:** correct regex to prevent double pipelines on tagged releases ([86aaf3f](https://gitlab.com/chet.manley/create-node-project/commit/86aaf3fd90f9125d2b60fe77bca2371edd402951))
* **ci:** ensure tagged versions always create pipelines ([9f43e96](https://gitlab.com/chet.manley/create-node-project/commit/9f43e969bc38c2bf7f8142f9bdf3ff8dd852cadd))

## [1.0.0-alpha.0](https://gitlab.com/chet.manley/create-node-project/compare/v0.0.3...v1.0.0-alpha.0) (2020-08-28)


### ⚠ BREAKING CHANGES

* simplify template configuration and use more descriptive variable names

### Bug Fixes

* **bug:** catch error when template files dir is missing or inaccessible ([d86461e](https://gitlab.com/chet.manley/create-node-project/commit/d86461ea57824ca0eb35e83e6cf55734c3ed44fa))
* **bug:** ignore missing template config files ([385b442](https://gitlab.com/chet.manley/create-node-project/commit/385b442906b72f609604cf7a8af22392b00a662b))
* **ci:** prevent double merge/tag pipelines on tag push ([c753fd7](https://gitlab.com/chet.manley/create-node-project/commit/c753fd76749ecd3f168f740fd100b8a063a9609e))


* simplify template configuration and use more descriptive variable names ([9ba81e6](https://gitlab.com/chet.manley/create-node-project/commit/9ba81e649f5dde14c099b823711b30ae1b3289e0))

### [0.0.3](https://gitlab.com/chet.manley/create-node-project/compare/v0.0.2...v0.0.3) (2020-08-28)


### Bug Fixes

* **bug:** `target directory is not empty` thrown by `processArgs` positional name change ([72dae3e](https://gitlab.com/chet.manley/create-node-project/commit/72dae3e3aabd677f5f8f6f1ffd87fd7a1f965150))
* **docs:** remove Gitlab-only markdown to fix NPM registry layout ([c2375e2](https://gitlab.com/chet.manley/create-node-project/commit/c2375e235a94bc6e056e407aa5b0ea0a7ee0a6ca))

### [0.0.2](https://gitlab.com/chet.manley/create-node-project/compare/v0.0.1...v0.0.2) (2020-08-27)


### Features

* handle `~/...` paths cross-platform ([b50f782](https://gitlab.com/chet.manley/create-node-project/commit/b50f782d8fbfea4c42d56ede11624c38dd56616b))


### Bug Fixes

* **bug:** removed wrong object ([5a92046](https://gitlab.com/chet.manley/create-node-project/commit/5a92046cee335de3eb7426a4d81195816c22c892))

### 0.0.1 (2020-08-27)


### Bug Fixes

* **bug:** await result of Project.init ([c72734c](https://gitlab.com/chet.manley/create-node-project/commit/c72734cc427382670e3a84ad6a7ff3d69e0a2daf))
* **bug:** do not assign targetPath to template instance ([b2b9035](https://gitlab.com/chet.manley/create-node-project/commit/b2b9035ced28bce2ec3400b882476c1461ef5efa))
* **bug:** make package entrypoint match proper CJS index file ([49b589c](https://gitlab.com/chet.manley/create-node-project/commit/49b589c798f8dbef939e3e6cc35940cd5fc3347c))
* **bug:** pass targetPath to Template.update method ([e8262e7](https://gitlab.com/chet.manley/create-node-project/commit/e8262e798f188ff467ea1cbdd49bc8bd7974e0d6))
* **bug:** prevent TypeError when command has null stdout ([58bbb6c](https://gitlab.com/chet.manley/create-node-project/commit/58bbb6cb665b4589b6f2c97fe260988fca609974))
* **bug:** removed quotes from command argument ([21e8245](https://gitlab.com/chet.manley/create-node-project/commit/21e824537bb80d10d87137d3d96648b927857d0b))
* **ci:** attempt to enable dependency scanner ([e771180](https://gitlab.com/chet.manley/create-node-project/commit/e7711803ef75036b56f66a7aa4cbb75b3ec9507e))
* **ci:** attempt to enable dependency scanner ([1577d44](https://gitlab.com/chet.manley/create-node-project/commit/1577d44132b46b2973c1d0ae69d191db2eda3581))
* **ci:** attempt to enable dependency scanner ([bb88e71](https://gitlab.com/chet.manley/create-node-project/commit/bb88e71a9d47c3dab7c00077e2174a1be0a6160d))
* **ci:** attempt to enable dependency scanner ([2c251c9](https://gitlab.com/chet.manley/create-node-project/commit/2c251c9701999e7d6bdb957dbd6024f59b4a44c7))
* **ci:** avoid duplicate names in stages ([691a697](https://gitlab.com/chet.manley/create-node-project/commit/691a6979dbb7c60c95a2bb47f7e65d8c15358c7f))
* **ci:** include paths must be from project root ([5179ea0](https://gitlab.com/chet.manley/create-node-project/commit/5179ea07f109b6c65dd705abc0bbbc673dde38b6))
* **ci:** move dependency scanner config into proper directory ([059546a](https://gitlab.com/chet.manley/create-node-project/commit/059546a20f563a856076d53332daafa4ee67ff6e))
* **ci:** move license compliance artifact report to Report stage ([e3637fa](https://gitlab.com/chet.manley/create-node-project/commit/e3637fa4868f6d6370893c40de2626917b0e7b04))
* **ci:** only install script dependencies where required ([ee8b362](https://gitlab.com/chet.manley/create-node-project/commit/ee8b3625b0562d8a5f43bf06137a0cd95acf5c37))
* **compat:** fs/promises incompatible in node <14 ([1c88f88](https://gitlab.com/chet.manley/create-node-project/commit/1c88f88367da08741d40bcb49c88cd7d450f9c74))
