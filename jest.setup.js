'use strict'

jest.mock('child_process')
jest.mock('fs')

const { toBeType } = require('jest-tobetype')
const stripAnsi = require('strip-ansi')

expect.addSnapshotSerializer({
  serialize: value => stripAnsi(value),
  test: value => Boolean(value) && typeof value === 'string'
})

expect.extend({
  toBeType
})
