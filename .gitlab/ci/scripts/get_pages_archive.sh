#!/usr/bin/env sh

# Script must always be run from project root.
cd "${CI_PROJECT_DIR}"
lib=$(readlink -f '.gitlab/ci/scripts/lib/')

. "${lib}/cleanup.sh"
. "${lib}/download_job_artifacts.sh"
. "${lib}/get_last_successful_pages_job.sh"

trap 'cleanup' EXIT

job=$(get_last_successful_pages_job)
ec=$?
if [[ $ec -ne 0 ]]; then
  if [[ $ec -eq 2 ]]; then
    echo "No previous 'pages' jobs found. Creating new 'public' directory." 1>/dev/stderr
    [[ -d "${CI_PROJECT_DIR}/public" ]] || mkdir "${CI_PROJECT_DIR}/public"
    exit $?
  fi
  echo "Previous pages job search failed. ${job}" 1>/dev/stderr
  exit $ec
fi

job_id=$(printf '%s' "${job}" | jq -cjM '.id')
archive=$(download_job_artifacts "${job_id}")
ec=$?
if [[ $ec -ne 0 ]]; then
  echo "Previous 'pages' archive failed to download. ${archive}" 1>/dev/stderr
  exit $ec
fi

unzip "${archive}" -d "${CI_PROJECT_DIR}"
ec=$?
if [[ $ec -ne 0 ]]; then
  echo "Archive decompression failed." 1>/dev/stderr
  exit $ec
fi

exit 0
