#!/usr/bin/env sh

# Script must always be run from project root.
cd "${CI_PROJECT_DIR}"
lib=$(readlink -f '.gitlab/ci/scripts/lib/')

. "${lib}/add_package_tag.sh"
. "${lib}/get_package_version.sh"
. "${lib}/get_published_tags.sh"

tag="${1}"

version=$(get_package_version)
ec=$?
if [[ $ec -ne 0 ]]; then
  echo 'Failed to retrieve package version.' 1>/dev/stderr
  exit $ec
fi

if [[ -n "${tag}" ]]; then
  add_package_tag "${version}" "${tag}"
  exit $?
fi

tags=$(get_published_tags "${version}")
ec=$?
if [[ $ec -ne 0 ]]; then
  echo "Failed to retrieve published tags for v${version}." 1>/dev/stderr
  exit $ec
fi

if printf '%s' "${tags}" | egrep -q '^(alpha|beta|rc)'; then
  add_package_tag "${version}" next
fi

exit $?
