#This script is meant to be sourced!

# `lib` must be set in sourcing script
. "${lib}/api_get.sh"

get_last_successful_pages_job () {
  local job=''
  local ec=0

  job=$(api_get 'jobs' \
    '[.[]? | select(.name == "pages")] | max_by(.finished_at) // empty' \
    'scope[]=success' \
  )
  ec=$?

  printf '%s' "${job}"

  [[ $ec -ne 0 ]] && return $ec
  return 0
}
