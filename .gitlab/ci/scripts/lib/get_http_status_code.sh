#This script is meant to be sourced!

get_http_status_code () {
  [[ -n "${1}" && -f "${1}" ]] || return 1
  local sc=$(sed -n 's/^HTTP[\/0-9]*\s\+\([0-9]\+\)/\1/p' ${1})

  printf '%s' "${sc:-'unknown'}"

  return 0
}
