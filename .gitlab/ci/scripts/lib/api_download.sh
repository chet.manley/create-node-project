#This script is meant to be sourced!

# `lib` must be set in sourcing script
. "${lib}/verify_api_response.sh"

api_download () {
  [[ -n "${1}" ]] || return 1
  local endpoint="${1}"
  local filepath="${2:-archive.zip}"
  local query="${3}"

  local server_error=''
  local status_code=''

  status_code=$(curl -sLw "%{http_code}" \
    -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    -d "${query}" \
    -X 'GET' "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/${endpoint}" \
    -o "${filepath}" \
  )

  # exit code 3: API request failed
  if [[ $status_code -ne 200 ]]; then
    server_error=$(verify_api_response "${filepath}")
    printf 'API error: %s' "${server_error:-unspecified}"
    return 3
  fi

  # exit code 2: could not find file
  [[ -f "${filepath}" ]] || return 2
  return 0
}
