#!/usr/bin/env sh

# Blocking: https://gitlab.com/gitlab-org/gitlab/-/issues/21543
# Cannot use local includes outside of a project:
# {"status":"invalid","errors":["Local file `.gitlab/ci/code-quality.yaml` does not have project!"]}
json=$(cat ./.gitlab-ci.yml | npx js-yaml | jq -cjM '{"content": "\(.)"}')

curl -sLH 'Content-Type: application/json' -d "${json}" -X POST "https://gitlab.com/api/v4/ci/lint"
