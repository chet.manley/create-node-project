'use strict'

const { fs } = require('memfs')

fs.promises.readFileSpy = jest.spyOn(fs.promises, 'readFile')
fs.promises.renameSpy = jest.spyOn(fs.promises, 'rename')
fs.promises.writeFileSpy = jest.spyOn(fs.promises, 'writeFile')

module.exports = exports = fs
