'use strict'

let error = null
let results = {}

const run = jest.fn(async _ => {
  if (error) { throw error }
  return results
})

function __setError (err) {
  error = err
}

function __setResults (number = 0) {
  results = {}

  for (let i = 0; i < number; i++) {
    results[i] = ''
  }
}

module.exports = exports = { run, __setError, __setResults }
