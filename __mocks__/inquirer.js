'use strict'

const mockInquirer = jest.createMockFromModule('inquirer')

async function mockPrompt (questions) {
  return questions.reduce((a, q) => {
    if (q.name) { a[q.name] = 'mock answer' }
    if (q.filter) { a[q.name] = q.filter(a[q.name]) }
    return a
  }, {})
}

mockInquirer.prompt = jest.fn(mockPrompt)

module.exports = exports = mockInquirer
