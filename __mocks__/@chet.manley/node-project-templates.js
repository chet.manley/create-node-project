'use strict'

const mockNPT = {
  config: {
    defaults: {
      manifestFile: 'pkg.json',
      templateName: 'base',
      templateFilesDir: 'files'
    }
  },
  path: '/tmp/mock-templates',
  templateDirs: ['base', 'tmpl1', 'tmpl2']
}

module.exports = exports = mockNPT
