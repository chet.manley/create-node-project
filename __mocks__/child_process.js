'use strict'

const EventEmitter = require('events')
const { Readable, Writable } = require('stream')

const childProcess = jest.requireActual('child_process')

const getStdioStreams = instance => [
  new Writable({
    write (chunk, encoding = 'utf8', callback) {
      instance.__stdin += chunk
      callback()
    }
  }),
  new Readable({
    read () {}
  }),
  new Readable({
    read () {}
  })
]

class MockChildProcess extends EventEmitter {
  constructor (cmd, args, options) {
    super()

    this.__cmd = cmd
    this.__args = args
    this.__options = options
    this.__stdin = ''

    this.connected = true
    this.exitCode = null
    this.killed = false
    this.signalCode = null

    this.stdio = getStdioStreams(this)
    this.stdin = this.stdio[0]
    this.stdout = this.stdio[1]
    this.stderr = this.stdio[2]

    return this
  }

  close () {
    if (!this.connected) { return }
    this.connected = false

    this.stdin.destroy()
    this.stdout.destroy()
    this.stderr.destroy()
    this.emit('close', this.exitCode, this.signalCode)
  }

  disconnect () {
    this.emit('disconnect')
    this.close()
  }

  error (err, code = 1) {
    this.exitCode = code

    this.emit('error', err)
    this.close()
  }

  exit (code = 0) {
    process.nextTick(_ => {
      this.exitCode = code

      this.emit('exit', this.exitCode, this.signalCode)
      this.close()
    })
  }

  kill (signal = 'SIGTERM') {
    this.killed = true
    this.signalCode = signal

    this.close()
  }
}

function mockSpawn (command, args, options) {
  return new MockChildProcess(command, args, options)
}

jest.spyOn(childProcess, 'spawn').mockImplementation(mockSpawn)

module.exports = exports = childProcess
